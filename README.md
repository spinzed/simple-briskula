# Briskula!
This is an in-browser implementation of italian card game called Briscola.  

## Technologies used
### Frontend Stack
- Language: TypeScript
- Framework: React
### Backend Stack
- Language: JavaScript
- App Server: NodeJS/Express w/ socket.io
- Caching database: Redis
- Web Server: nginx
### Deployment Tools
- Docker

## To run dis

Step 1:
```shell
git clone https://gitlab.com/Spinzed/simple-briskula.git
```

Step 2:
```shell
cd simple-briskula
```

Step 3:
```shell
docker-compose up
```

Step 4:
Go to `localhost` (default port 80) in your favorite browser.  

And boom that's it, easy as that.
