import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  root: "./client",
  publicDir: "client/static",
  build: {
    outDir: "../server/public",
  },
  plugins: [react()],
});
