import { createClient } from "redis";
import Logger from "../settings/logger.js";
import { REDIS_HOST, REDIS_PORT } from "../settings/settings.js";

const logger = new Logger("Redis");

// redis client initialization
const client = createClient({ host: REDIS_HOST, port: REDIS_PORT });
client.on("error", (error) => {
  logger.error("Error while creating redis client:", error);
});

export const Client = client;
