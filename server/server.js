import express, { urlencoded, json, static as staticFolder } from "express";
import mst from "mustache-express";
import indexRouter from "./routes/routes.js";
import compression from "compression";
import { createSocket } from "./socket/socket.js";
import Logger from "./settings/logger.js";
import { SERVER_PORT } from "./settings/settings.js";
import { fileURLToPath } from 'url';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

// server logger
const logger = new Logger("Server");

// express server initalization
const app = express();
const server = app.listen(SERVER_PORT, () => {
  logger.info("Server started at port", SERVER_PORT);
});

// bind the socketio instance to the express server
createSocket(server);

app.use((_, res, next) => {
  res.setHeader("Service-Worker-Allowed", "/");
  next();
});

// settings
app.use(urlencoded({ extended: true }))
app.use(json()); // Why tf is this even necesarry?
app.use(compression()); // compress everythin to gzip
app.use(staticFolder(__dirname + "/public", { index: false }));
app.engine("html", mst());
app.set("view engine", "html");
app.set("views", __dirname + "/public")

// route mapper
app.use("/", indexRouter);

// setInterval(() => {
//   const used = process.memoryUsage().heapUsed / 1024 / 1024;
//   console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
// }, 1000)

export default { app };
