import { v4 as uuidv4 } from "uuid";
import { Client } from "../redis/client.js";
import * as PlayerConnection from "./PlayerConnection.js";
import Logger from "../settings/logger.js";
import { MODES } from "../settings/constants.js";

const logger = new Logger("Lobby");

class Lobby {
  constructor(id, mode) { // id - typeof string | undefined, mode - typeof Mode
    this.__LOBBY = []; // typeof PlayerConnection[]
    this.mode = mode; // typeof Mode
    this.MAXPLAYERS = mode.maxPlayers;
    this.id = id || uuidv4();

    // clear the space in Redis in case there is smth there
    this.removeFromRedis();
    Client.rpush(this.id, this.mode.name);
  }

  get occupancy() {
    return this.__LOBBY.length;
  }

  get players() {
    return this.__LOBBY;
  }

  // THIS METHOD IS USED ONLY WHEN PULLING THE LOBBY FROM REDIS
  // PARAMETERS: player - instanceof PlayerConnection
  UNSAFE_add_player(player) {
    this.__LOBBY.push(player);
    Client.rpush(this.id, player.id);
  }

  // adds new players, player is of type
  // PARAMETERS: player - instanceof PlayerConnection
  // RETURN VALUE: boolean - did the player joining succeed
  join(player) {
    // in case that lobby exceeds max number of play
    if (this.__LOBBY.length >= this.MAXPLAYERS) {
      logger.error("Max players exceeded in lobby", this.uuid);
      return false;
    }
    if (!player) {
      logger.error(`Cannot add an undefined player to lobby ${this.uuid}`);
      return false;
    }

    if (this.__LOBBY.some(p => p.id === player.id)) {
      const { id } = player.socket;
      logger.error(`Player ${id} already added to lobby ${this.uuid}`);
      return false;
    }

    // add the player to the lobby AFTER ALL CHECKS HAVE BEEN DONE
    this.UNSAFE_add_player(player);

    // this is necessary to correctly remove the player on dc
    player.updateData({ lobby: this.id });

    // return successful adition of player to lobby
    return true;
  }

  // disconnects the player
  // PARAMETERS: player - instanceof PlayerConnection
  // RETURN VALUE: boolean - if the dc is successful
  disconnect(player) {
    if (!player) {
      logger.error(`Cannot dc an undefined player in lobby ${this.uuid}`);
      return;
    }
    const filteredPlayer = this.__LOBBY.find(p => p.id === player.id);

    if (!filteredPlayer) {
      logger.error(`Cannot dc player ${player.socket.id} in lobby ${this.uuid}`);
      return false;
    }

    // remove the player AFTER ALL CHECKS HAVE BEEN PERFORMED
    this.__LOBBY.filter(v => v.id !== filteredPlayer.id);

    // TODO: move this to the RedisController
    Client.lrem(this.id, 1, player.id);
    player.updateData({ lobby: undefined });

    return true;
  }

  // RETURN VALUE: boolean
  canLaunch() {
    return this.__LOBBY.length >= this.MAXPLAYERS;
  }

  // used for launching a game through GameManagment
  // RETURN VALUE: [players: typeof PlayerConnection[], mode - typeof Mode]
  gameData() {
    return [[...this.__LOBBY], this.mode]
  }

  // RETURN VALUE: boolean was everything performed successful
  close() {
    // I had remove the reference from the original array because it wouldn't
    // remove them all that way
    [...this.__LOBBY].forEach(v => this.disconnect(v));

    this.removeFromRedis()

    return true;
  }

  // Load a lobby from redis.
  // additionalPlayer is type of PlayerConnection, it is usually a player that has just dc'd
  // so it cannot be found in the io instance, so is is passed manually like this. It is a wonky
  // solution, but fuck it. CAN BE UNDEFINED
  //
  // RETURN VALUE: lobby as a promise. If the lobby is undefined, that means it doesn't exist.
  static async loadFromRedis(id, io, additionalPlayer) {
    return new Promise((res, rej) => {
      // find an array with id of the lobby, from the first element to the last
      // first element is max playes and the rest are players
      Client.lrange(id, 0, -1, async (err, result) => {
        if (err) {
          logger.error("Error while loading player from redis:", err);
          rej(err);
          return;
        }
        const resultNoRef = [...result];

        // lobby with that id doesn't exist, resolve with undefined to signal that
        if (resultNoRef.length === 0) {
          res(undefined);
          // don't forget to return, this is a promise and resolving
          // won't stop further exec
          return;
        }

        // take the mode from the beginning of the array
        const modeName = resultNoRef.shift();
        const mode = MODES.find(m => m.name === modeName);

        // shift() will take the first element out of the array and return it
        const lobb = new Lobby(id, mode);

        // the rest must be player ids
        for (let playerID of resultNoRef) {
          // check if playerID is of the passed in additional player that is not
          // in the socketio instance, but only if it's been passed.
          if (additionalPlayer && playerID === additionalPlayer.id) {
            lobb.UNSAFE_add_player(additionalPlayer);
            continue;
          }

          const player = await PlayerConnection.loadFromRedis(playerID, io);
          if (player === undefined) {
            logger.error(`Player ${playerID} is not defined in lobby ${id}`)
            continue
          }

          // here there's no need to call lobby.join because all the checks
          // have been performed and other stuff has been updated
          lobb.UNSAFE_add_player(player);
        }

        res(lobb);
      });
    });
  }

  removeFromRedis() {
    Client.ltrim(this.id, 1, 0);
  }
}

export { Lobby };
