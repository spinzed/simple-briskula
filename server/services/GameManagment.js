import { Game } from "../game/game.js";
import { Lobby } from "./Lobby.js";
import { PlayerConnection } from "./PlayerConnection.js";
import { Client } from "../redis/client.js";
import { RedisSessionController } from "./RedisSessionController.js";
import { MODES } from "../settings/constants.js";
import Logger from "../settings/logger.js";

const logger = new Logger("Manager");

class GameManagment {

  // PARAMETERS: io - socket.io instance
  constructor(io) {
    this.redisManager = new RedisSessionController(Client);
    this.io = io;

    this.redisManager.flush();
  }

  // PARAMETERS: socket - socket instance
  openConnection(socket) {
    const pleya = new PlayerConnection(socket, this.io);
    this.redisManager.registerConnection(pleya);

    return pleya;
  }

  // closes a PlayerConnection. will not return errors if the PlayerConnection is invalid
  // PARAMETERS: connectionID - string of type socket.id
  async closeConnection(connection) {
    // remove it from all queues and stuff
    connection.queues.length > 0 && this.leaveQueue(connection);
    connection.lobby && this.leaveLobby(connection.lobby, connection);

    this.redisManager.unregisterConnection(connection);
  }

  // generate a 6 digit/letter ID for a lobby
  async generateID() {
    const existing = await this.redisManager.getLobbies()
    let id;
    // first time ever that I use do while in js
    do {
      id = this.__ID();
    } while (existing.includes(id));

    return id;
  }

  __ID() {
    const vals = "abcdefghijklmnopqrstuvwxyzABCDEFGIJKLMNOPQRSTUVWXYZ1234567890"; // yeys hardcode
    return new Array(6).fill("").map(() => {
      const index = Math.floor(Math.random() * vals.length);
      return vals[index];
    }).join("");
  }

  // PARAMETERS: mode - string
  async createLobby(modeName) {
    const mode = MODES.find(m => m.name === modeName);
    const newLobby = new Lobby(await this.generateID(), mode);
    this.redisManager.registerLobby(newLobby);

    return newLobby;
  }

  // PARAMETERS: lobbyID - string of type uuid4, player - instanceof PlayerConnection
  // RETURN VALUE: ("err" | "joinError" | undefined) - if the player joins sucessfully
  async joinLobby(lobbyID, player) {
    const lobby = await Lobby.loadFromRedis(lobbyID, this.io);

    if (!lobby) {
      logger.error(`Player with ID ${player.id} asked for unexisting lobby ${lobbyID}`);
      return "joinError";
    }

    // remove from already existing lobby if any
    if (player.lobby) this.leaveLobby(player.lobby, player);

    // remove from if the player's in one
    if (player.inQueue()) this.leaveQueue(player);

    /* the return value signals if the player joined without problems (bool)
      message display is handled inside the lobby.join() function */
    const hasJoined = lobby.join(player);
    if (!hasJoined) return "err";
    
    // if there is already one player, he's the master player, send him updates about players joined
    lobby.players.length > 0 && this.sendLobbyPlayerUpdate(lobby, player);

    if (lobby.mode.maxPlayers === 4) return undefined;

    // if the game is not 2v2, start it automatically
    this.startLobbyGame(lobby);
  }

  // PARAMETERS: player - instanceof PlayerConnection, lobbyID - string
  async leaveLobby(lobbyID, player) {
    const lobby = await Lobby.loadFromRedis(lobbyID, this.io, player);

    if (!lobby) {
      logger.warn(`Lobby of id ${lobbyID} doesn't exist`);
      return false;
    }

    const outcome = lobby.disconnect(player);

    if (!outcome) return false;

    // automatically close the lobby if it is empty
    if (lobby.occupancy === 0) {
      return this.closeLobby(lobby);
    }

    // if there is already one player, he's the master player, send him updates about players joined
    lobby.players.length > 0 && this.sendLobbyPlayerUpdate(lobby, undefined, player);

    return true;
  }

  // Send player joined/left to the master socket in a lobby.
  // PARAMETERS: lobby - typeof Lobby, joined, left - typeof PlayerConnection
  sendLobbyPlayerUpdate(lobby, joined, left) {
    joined && lobby.players[0].socket.emit("gameStart", { type: "lobbyUpdate", payload: { playerIn: { id: joined.id, name: joined.name } }});
    left && lobby.players[0].socket.emit("gameStart", { type: "lobbyUpdate", payload: { playerOut: { id: left.id, name: left.name } }});
  }

  async startLobbyGameAs(player, opts) {
    const lobby = await Lobby.loadFromRedis(player.lobby, this.io);
    if (!lobby) {
      logger.warn(`Lobby of id ${player.lobby} doesn't exist`);
      return "err";
    }
    // if the socket calling isn't the master player (the first one in the array), abort
    if (lobby.players[0].id !== player.id) return "unauth";
    return this.startLobbyGame(lobby, opts);
  }
  
  // PARAMTERS - lobbyID - typeof string, player - typeof connection, the master connection
  //           - opts - { random: boolean, teams: string[] }
  // RETURN VALUE - ("err" | "unauth" | undefined) - whether the function has succeeded
  async startLobbyGame(lobby, opts) {
    if (!lobby) {
      logger.error(`Unexisting lobby passed to start`);
      return "err";
    }

    // after player is joined and made sure everything is alright,
    // check if its possible to create a game, return true if it isnt
    if (!lobby.canLaunch()) return "err";

    // check whether every player in the opts.teams array is actually joined
    if (opts && opts.teams) {
      if (opts.teams.flat().length !== lobby.players.length) {
        logger.error("Not enough players to start a lobby", opts.teams, lobby.players.map(p => p.id));
        return "err";
      }
      if (!lobby.players.map(p => p.id).every(p => opts.teams.flat().includes(p))) {
        logger.error("One of more of the players is not in the lobby", opts.teams, lobby.players.map(p => p.id));
        return "err";
      }
    }

    // launch the game and close the lobby
    this.launchGame(...lobby.gameData(), opts);
    this.closeLobby(lobby);

    // since there were no errors, return undefined
    return undefined;
  }

  // PARAMATERES: lobby - typeof Lobby
  // RETURN VALUE: was the lobby removing successful
  closeLobby(lobby) {
    if (!lobby) {
      logger.error(`Lobby of id ${lobby.id} doesn't exist`);
      return false;
    }

    lobby.close();
    this.redisManager.unregisterLobby(lobby);

    return true;
  }

  // PARAMETERS: player - typeof PlayerConnection, modes - typeof string[]
  queuePlayer(player, modes) {
    if (player.lobby) {
      this.leaveLobby(player.lobby, player)
    }
    this.leaveQueue(player);

    this.redisManager.registerToQueue(player, modes);
    player.updateData({ queues: modes });
  }

  // PARAMETERS: player - typeof PlayerCOnnection
  leaveQueue(player) {
    this.redisManager.unregisterFromQueue(player);

    const wasInQueue = player.queue && player.queue.length > 0;
    player.updateData({ queues: [] });

    // return whether was the player in queue
    return wasInQueue;
  }

  // PARAMETERS: num - number, number of players to pop from queue, mode - Mode
  // RETURN VAL: players - PlayerConnection[]
  async popFromQueue(num, mode) {
    const queue = await this.redisManager.getQueue(mode.name);

    // check if there's enough players
    if (queue.length < num) {
      logger.error(`Not enough players for queue, want ${num} but have ${queue.length}`);
    }

    // return value: PlayerConnection array
    const players = [];

    // pick num of elements, reconstruct them from redis, remove them from queue and 
    for (let i = 0; i < num; i++) {
      const index = GameManagment.randInt(queue.length);
      const playerID = queue.splice(index, 1)[0];

      const player = await PlayerConnection.loadFromRedis(playerID, this.io);
      this.leaveQueue(player);

      players.push(player);
    }

    // return the players array
    return players;
  }

  static randInt(num) {
    return Math.floor(num * Math.random());
  }

  async attemptMatchAllModes() {
    for (let i = 0; i < MODES.length; i++) {
      const mode = MODES[i];
      const matchFound = await this.attemptRandomMatch(mode);

      // if a match if found, there's no need to continue queueing
      if (matchFound) return;
    }
  }

  // matchmake if 2 players are available
  // PARAMETERS: mode - string
  // RETURN VALUE: boolean - was the matchmatking successful
  async attemptRandomMatch(mode) {
    const queueLength = await this.redisManager.getQueueLength(mode.name);

    // get required players for mode from constants
    const requiredPlayers = mode.maxPlayers;

    // if there are less players than that modes's player requirement, return 
    if (queueLength < requiredPlayers) return false;

    // pop required number of players
    const players = await this.popFromQueue(requiredPlayers, mode);

    // safety check whether any of the players is undefined which shouldn't happen
    if (players.includes(undefined)) {
      logger.error(`An undefined player is popped from queue`);
      return;
    }

    this.launchGame(players, mode);

    return true;
  }

  // initiate a new game and remove it when it finishes,
  // this is the ONLY place where a game may be initiated.
  // PARAMETERS: players - typeof Connection[], mode - typeof string
  launchGame(players, mode, opts) {
      const game = new Game({
        io: this.io,
        players,
        mode,
        teams: opts && opts.teams,
        onPlayerDCD: this.markPlayerDCd.bind(this),
        onRequestConn: this.getDCdPlayerConn.bind(this),
        onEnd: () => {
          this.redisManager.unregisterGame(game);
          this.redisManager.clearGameDCdData(game.id);
        }
      });
      this.redisManager.registerGame(game);
  }

  // PARAMETERS: gameID - typeof string, key - typeof string
  markPlayerDCd(gameID, key) {
    this.redisManager.markDCd(gameID, key);
  }

  // PARAMETERS: gameID - typeof string, key - typeof string
  async getDCdPlayerConn(gameID, key) {
    const playerID = await this.redisManager.getDCdSavedID(gameID, key);

    // if it doesn't exist, return
    if (!playerID) return undefined;

    const playerCon = await PlayerConnection.loadFromRedis(playerID, this.io);

    // if the player isn't undefined, everything went alright and the player can be
    // removed from the list of dcd
    if (playerCon !== undefined) {
      this.redisManager.removeDCdPlayer(gameID, key)
    }
    return playerCon;
  }

  // PARAMETERS: playerID - typeof string, gameID - typeof string, key - typeof string
  async checkReconnect(playerID, gameID, key) {
    const player = await PlayerConnection.loadFromRedis(playerID, this.io);
    // if something went wrong, return false
    if (!player) return false;
    
    // if the key is found in redis, update it with the player's ID
    if (await this.redisManager.isKeyDCd(gameID, key)) {
      this.redisManager.setDCdData(gameID, key, playerID);
      return true;
    }
    // nothing found, return false
    return false;
  }
}

export { GameManagment };
