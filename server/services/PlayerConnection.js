import isEqual from "lodash/isEqual.js";
import Logger from "../settings/logger.js";
import { Client } from "../redis/client.js";
import { pick } from "../settings/utils.js";

const logger = new Logger("PlrConn");

/* this are the default settings for the class, this is also
  used to upload class data to Redis */
const __DEFAULT = {
  queues: [],
  lobby: undefined,
  game: undefined,
  name: undefined,
};

class PlayerConnection {
  constructor(socket, io, persist) {

    this.io = io;
    this.socket = socket;
    this.id = socket.id;

    // apply the default settings without the references messing up
    Object.assign(this, { ...__DEFAULT });

    // update the Redis db, but dont if persist is truthy
    if (!persist) {
      this.loadToRedis(Client);
    }
  }

  // boolean, reports whether the player is in any of the queues
  inQueue() {
    return this.queues.length > 0;
  }

  updateData(settings) {
    // pick only the wanted data (in case of wrongly passed data)
    const data = pick(settings, Object.keys(__DEFAULT));

    // assign the new settings to the object
    Object.assign(this, data);

    // update the Redis db
    this.loadToRedis();
  }

  loadToRedis() {
    // get an arrays of names of object properties which need to be cached to Redis
    const keys = Object.keys(__DEFAULT);

    /* call a lodash function which will pick only the values that are needed, after
    then use Array.prototype.flat() to flat the array so it can be passed to Redis */
    const pickedProps = Object.entries(pick(this, keys));
    const flattenedProps = pickedProps.flat();

    // this is needeed since redis doesnt accept undefined
    const noUndefinedProps = flattenedProps.map(v => String(v))

    // write the props to Redis under the sign of socket.id
    Client.hmset(this.id, ...noUndefinedProps);
  }

  static loadFromRedis(id, io) {
    return new Promise((res, rej) => {
      Client.hgetall(id, (err, values) => {
        if (err) {
          logger.error("Cannot load from Redis:", err);
          rej(err);
          return;
        }
  
        // check whether all data has been loaded
        if (!isEqual(Object.keys(values), Object.keys(__DEFAULT))) {
          logger.error("Could not load values for player", id);
          rej("values from Redis did not load for " + id);
          return;
        }


        // convert undefined strings to undefined
        const valueFiltered = {...values};
        for (const [key, val] of Object.entries(valueFiltered)) {
          valueFiltered[key] = val === "undefined" ? undefined : val; 
        }

        // find the socket to bind it to player
        const socket = io.sockets.sockets.get(id);
        
        // [!!!] player not found, something is wrong
        if (!socket) {
          logger.error("Could not find socket for player with ID", id);
          res(undefined);
          return;
        }
  
        // make a new player instance
        const pleya = new PlayerConnection(socket, io, true);
        pleya.updateData(valueFiltered);

        // had to update the queues manually since it is parsed to string
        pleya.queues = values.queues.split(",");

        // return the player
        res(pleya);
      });
    })
  }

  removeFromRedis() {
    Client.hdel(this.id, Object.keys(__DEFAULT));
  }

}

export { PlayerConnection };
