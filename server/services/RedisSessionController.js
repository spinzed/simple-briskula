import { MODES } from "../settings/constants.js";
import Logger from "../settings/logger.js";

const logger = new Logger("RedisCTL");

class RedisSessionController {

  // PARAMETERS: client - typeof RedisClient
  constructor(client) {
    /* 
      There are 4 Redis hash tables:
      - CONN - for connections, contains connection data by their id. Each
        connection is also saved individually in its own hash
      - QUEUE - contains connection ids for every conn that is in queue
      - GAMES - contains ids of every game
      - LOBBIES - contains lobby ids. Each lobby has its own hash
    */

    this.__CONN = "__###owo###CONNECTIONS";
    this.__QUEUE = "__###owo########QUEUE";
    this.__GAMES = "__####owo#######GAMES";
    this.__DCDPL = "__#####owo######DCDPL";
    this.__LOBBIES = "__####owo###LOBBIES";

    this.CLIENT = client;
  }

  flush() {
    logger.info("Flushing DB...");
    this.CLIENT.flushdb();
  }

  // PARAMETERS: connection - typeof PlayerConnection
  registerConnection(connection) {
    if (connection === undefined) {
      logger.error("Cannot register an undefined connection");
      return;
    }
    this.CLIENT.sadd(this.__CONN, connection.id);
  }

  // PARAMETERS: connection - typeof PlayerConnection
  unregisterConnection(connection) {
    if (connection === undefined) {
      logger.error("Cannot unregister an undefined connection");
      return;
    }
    this.CLIENT.srem(this.__CONN, connection.id);
  }

  // check if a connection exists
  // PARAMETERS: connID - typeof connID
  checkConnection(connID) {
    return new Promise((res) => {
      this.CLIENT.sismember(this.__CONN, connID, (err, result) => {
        if (err) logger.error("Error while checking connection:", err);
        res(!!result);
      });
    })
  }

  // PARAMETERS: connection - typeof PlayerConnection, modes - typeof string[]
  registerToQueue(connection, modes) {
    modes.forEach(modeName => {
      if (!MODES.find(m => m.name === modeName)) {
        logger.error("Unsupported game mode:", modeName);
      }
      this.CLIENT.sadd(this.__QUEUE + modeName, connection.id);
    });
  }

  // PARAMETERS: connection - typeof PlayerConnection, modes - typeof string[]
  unregisterFromQueue(connection) {
    connection.queues.forEach(modeName => {
      if (!MODES.find(m => m.name === modeName)) {
        logger.error("Unsupported game mode:", modeName);
      }
      this.CLIENT.srem(this.__QUEUE + modeName, connection.id);
    });
  }

  // gets the entire queue aka all player ids
  // PARAMETERS: mode - string
  getQueue(mode) {
    return new Promise((res) => {
      this.CLIENT.smembers(this.__QUEUE + mode, (err, result) => {
        if (err) logger.error("Error while fetching queue:", err);
        res(result || []);
      })
    });
  }

  // gets the length from the entire queue
  // PARAMETERS: modeName - string
  getQueueLength(modeName) {
    return new Promise((res) => {
      this.CLIENT.scard(this.__QUEUE + modeName, (err, result) => {
        if (err) logger.error("[RedisC ERR] Error while fetching queue length:", err);
        res(result || 0);
      })
    });
  }

  // PARAMETERS: lobby - typeof Lobby
  registerLobby(lobby) {
    this.CLIENT.sadd(this.__LOBBIES, lobby.id);
  }

  // PARAMETERS: lobby - typeof Lobby
  unregisterLobby(lobby) {
    this.CLIENT.srem(this.__LOBBIES, lobby.id);
  }

  // check if a lobby exists
  // PARAMETERS: lobbyID - typeof string (uuidv4 lobby ID)
  checkLobby(lobbyID) {
    return new Promise((res) => {
      this.CLIENT.sismember(this.__LOBBIES, lobbyID, (err, result) => {
        if (err) logger.error("Error while checking lobby:", err);
        res(!!result);
      });
    })
  }

  // RETURN VALUE: lobby id list - typeof []string 
  getLobbies() {
    return new Promise((res) => {
      this.CLIENT.smembers(this.__LOBBIES, (err, result) => {
        if (err) logger.error("Error while fetching lobbies:", err);
        res(result || []);
      })
    });
  }

  // PARAMETERS: game - typeof Game
  registerGame(game) {
    this.CLIENT.sadd(this.__GAMES, game.id);
  }

  // PARAMETERS: game - typeof Game
  unregisterGame(game) {
    this.CLIENT.srem(this.__GAMES, game.id);
  }

  // PARAMETERS: gameID - string, key - string
  markDCd(gameID, key) {
    this.setDCdData(gameID, key, "");
  }

  // PARAMETERS: gameID - string, key - string, socketID - string
  setDCdData(gameID, key, socketID) {
    if (this.getDCdSavedID) {
      this.removeDCdPlayer(gameID, key, socketID);
    }
    this.CLIENT.sadd(this.__DCDPL + gameID, `${key} + ${socketID}`);
  }

  // PARAMETERS: gameID - string, key - string
  async removeDCdPlayer(gameID, key) {
    const socketID = await this.getDCdSavedID(gameID, key);
    this.CLIENT.srem(this.__DCDPL + gameID, `${key} + ${socketID}`);
  }

  // PARAMETERS: gameID - string, key - string
  async isKeyDCd(gameID, key) {
    const res = await this.getDCdSavedID(gameID, key);
    return res !== undefined;
  }

  // PARAMETERS: gameID - string, key - string
  getDCdSavedID(gameID, key) {
    return new Promise((res) => {
      this.CLIENT.smembers(this.__DCDPL + gameID, (err, result) => {
        if (err) {
          logger.error("Error while checking game:", err);
          res(undefined);
        }
        result.forEach(val => {
          const [k, playerID] = val.split(" + ");
          if (k === key) res(playerID);
        })
        res(undefined);
      });
    })
  }

  // PARAMETERS: gameID - string
  clearGameDCdData(gameID) {
    this.CLIENT.del(this.__DCDPL + gameID);
  }
}

export { RedisSessionController };
