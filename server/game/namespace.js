import Logger from "../settings/logger.js";

const logger = new Logger("Namespc");

const configureNamespace = (io, uuid, connectedPlayers) => {
  // idk is it a good idea to pass connectedPlayers like this, but Ima
  // do it anyway who gonna stop me, circular importing?

  const path = "/game/" + uuid;
  const nsp = io.of(path);

  // value can be a socket or an array of sockets
  nsp.as = (value) => {
    !value && logger.warn("Passed value is undefined");
    const broadcast = (channel, data) => {
      // socket is guaranteed to be defined
      connectedPlayers.getDefinedSockets().forEach(socket => {
        // if the value is an array
        if (Array.isArray(value)) {
          // if there isn't a socket with the desired socket id, emit
          if (!value.map(v => v.id).contains(socket.id)) {
            socket.emit(channel, data);
          }
        }
        // if the value is a single socket OR undefined
        if ((value && value.id) !== socket.id) {
          socket.emit(channel, data);
        }
      });
    }
    return { broadcast };
  }

  return nsp;
}

export { configureNamespace };
