import { v4 as uuidv4 } from "uuid";
import Logger from "../settings/logger.js";
import { zip } from "../settings/utils.js";

const logger = new Logger("PlayerC");

class Players {
  constructor(maxplayers) {
    /* 
      this.players is in format {
        globalSocket: socket.io socket, // will change if the player DCs
        conn: PlayerConnection, // will change if the player DCs
        socket: socket.io nsp socket,
        id: uuidv4 string,
        privateKey: uuidv4 string, // used for auth if the player DCs
        name: string,
        team: number, // name of the team doesn't matter
        wins: number,
        loses: number,
        totalPoints: number,
        totalEnemyPoints: number,
        cards: cards[],
        cardStack: cards[],
        ready: boolean,
        dcd: boolean, // is the player disconnected
        listener: () => void, // listener for disconnect
      }
    */  
    this.players = [];
    this.maxplayers = maxplayers;
    this.currentIndex = 0;
  }

  getAll() {
    return this.players;
  }

  reset() {
    this.currentIndex = 0;
    this.players.forEach(p => {
      p.cards = [];
      p.cardStack = [];
    });
  }

  // index of element with the desired socket id, if the element doesn't exist, return undefined
  indexOf(socketID) {
    let index = undefined;
    this.players.forEach((v, i) => {
      if (v.socket.id === socketID) index = i;
    });
    return index;
  }

  // returns the original object, not a copy so its safe to update it,
  // but not to recklessly change it
  getData(socketID) {
    return this.players.find(data => data.socket.id === socketID);
  }

  // gets only nsp sockets which are defined
  getDefinedSockets() {
    return this.players.map(data => data.socket).filter(s => s !== undefined);
  }

  // adds a new player to the this.players array and returns the new players object
  addPlayer(conn, name, team) {
    // if the max players limit has been exceeded, there shouldn't be any more players
    // thus somewhere an error has happened
    if (this.players.length >= this.maxplayers) {
      logger.error("Too many players");
      return;
    }

    const pl = {
      globalSocket: conn.socket,
      conn,
      socket: undefined,
      id: uuidv4(),
      key: uuidv4(),
      name,
      team,
      wins: 0,
      losses: 0,
      totalPoints: 0,
      totalEnemyPoints: 0,
      cards: [],
      cardStack: [],
      ready: false,
      dcd: false
    };
    this.players.push(pl);
    
    return pl;
  }

  // shuffle the players array
  shuffle() {
    const copy = [ ...this.players ];
    const newOrder = [];

    for (let i = 0; i < this.players.length; i++) {
      const index = Math.floor(copy.length * Math.random());
      const pl = copy.splice(index, 1)[0];
      newOrder.push(pl);
    }
    this.players = newOrder;
  }

  // PARAMETERS: arr - typeof string[n][n]
  synchronise(arr, maxTeams) {
    const newOrder = [];

    zip(arr).forEach((plID, i) => {
      const player = this.players.find(p => p.globalSocket.id === plID);
      player.team = i % maxTeams;
      newOrder.push(player);
    });

    this.players = newOrder;
  }

  rotate() {
    this.players.push(this.players.shift());
  }

  // rotate the array so that the first player is the one with desired ID
  setFirstPlayer(playerID) {
    // if the length of the players array is 0, return because this may be an error
    if (this.players.length === 0) {
      logger.warn("Cannot rotate the list because the player list is empty");
      return;
    }

    // if playerID is undefined, return
    if (playerID === undefined) {
      logger.error(`Cannot set first player because the player ID is undefined`);
      return;
    }

    // used to controll how many times the list has been rotated to prevent infinite loops
    let rotationNumber = 0;

    // rotate while the first element is not the desired one
    while (this.players[0].id !== playerID) {
      const firstElem = this.players.shift();
      this.players.push(firstElem);

      // check how many times has the array been rotated, if it has been rotated
      // too many times, return and log an error
      if (++rotationNumber >= this.players.length) {
        logger.warn(`The player with ID ${playerID} is not in the list`);
        return;
      }
    }
  }

  get current() {
    return this.players[this.currentIndex];
  }

  isLastPlayer() {
    return this.currentIndex === this.players.length - 1;
  }

  switchToNext() {
    this.currentIndex++;
    if (this.currentIndex >= this.players.length) this.currentIndex = 0;
  }
}

export {
  Players
};
