import { v4 as uuidv4 } from "uuid";
import { configureNamespace } from "./namespace.js";
import { CARDS } from "../settings/constants.js";
import { Players } from "./players.js";
import { structuredClone } from "../settings/utils.js";
import Logger from "../settings/logger.js";

const logger = new Logger("Game");

class Game {
  constructor(settings) {
    const { io, players, mode, teams, onPlayerDCD, onRequestConn, onEnd } = settings;
    // io is socket.io instance
    // players is of type Connection[] -- used only to initially send to the players the game data
    // mode is of type Mode (see constants.js)
    // teams is externaly-defined teams structure, can be undefined
    // onRequestConn is used when a player is reconnecting for verification that it's really him
    // onEnd is a callback function which is executed when the game is done
    // each individual card is in format { sign, number }

    this.io = io;
    this.id = uuidv4();

    logger.info(`Initiating new game w/ ID ${this.id}, mode ${mode.name} and players ${players.map(v => v.name)}`);

    // mode-game data object
    this.mode = mode;

    // externaly-defined teams structure, can be undefined
    this.TEAMCONFIG = teams;

    // callback to be called after the game ends
    this.onEnd = onEnd;
    // callback to be called when a player DCs
    this.markPlayerDCd = onPlayerDCD;
    // callback to be called when trying to get the new PlayerConnection of a DCd player
    this.requestConnection = onRequestConn;

    // if the mode is double, 2 turns must pass to calculate the winner.
    // To enable extra turns, this.extraHand must be defined
    if (this.mode.name.includes("dupla")) this.extraHand = false;

    // mark the original start time of the game
    this.originalStartTime = new Date();

    // make a players object which will hold all players' data
    this.players = new Players(this.mode.maxPlayers);

    // set the initial game values. They will also be reset if the game restarts
    this.setInitialValues();

    // tie this game to the each player and add player to this.players object
    players.forEach((conn, i) => {
      conn.updateData({ game: this.id });

      // choose a team for the player, this assures that there will be number of teams
      // equal to the respective mode
      const team = i % this.mode.maxTeams;
      
      // add the new player to the structure. The passed socket is will be updated
      // when the client responds
      const pl = this.players.addPlayer(conn, conn.name, team);

      this.setupDCListener(pl);
    });

    // used to broadcast event only to the players in this game
    this.nsp = configureNamespace(io, this.id, this.players);

    // wait for player's reponse for the message below
    // when all players respond, start the game
    this.listenConnections();

    // signal all players that the game has started, the function above will wait for confirmation from all players
    for (let i = 0; i < this.players.getAll().length; i++) {
      // the current player is always the first one in the this.players array
      const player = this.players.getAll()[0];

      // the player's teammates and opponents
      const players = [];

      this.players.getAll().forEach((p, i) => {
        // skip the first player because he's the one who'll recieve the update
        if (i === 0) return;

        // push in the players array, the player is teammate if the index is even
        players.push({ id: p.id, name: p.name, isTeammate: i % 2 === 0})
      });

      // send the changes to the first player in the array
      player.globalSocket.emit("gameStart", {
        type: "gameReady",
        payload: {
          mode: this.mode.name,
          uuid: this.id,
          playerID: player.id, // public ID
          key: player.key,    // private ID
          startTime: this.startTime.getTime(),
          players,
        }
      });

      // switch the current player to the next one. Hopefully, there will be switches as much as there
      // are players, so at the end of the loop the this.players array should remain unchanged
      this.players.setFirstPlayer(this.players.getAll()[1].id)
    }
  }

  // This will be run when the game starts or restarts.
  setInitialValues() {
    this.cards = { deck: structuredClone(CARDS) };
    this.isStarted = false;
    this.round = 0;
    this.started = false; // true only until the game runs, gets set to false on game end
    this.done = false; // only true when the game has ended. Will be set to false again if the game restarts
    this.startTime = new Date();

    // contains IDs of players that accepted a rematch, active only when the game is ended
    this.rematchPlayers = [];

    // dictates whether should turns from all players be blocked
    this.turnsDisabled = false;

    // keeps track of cards that are currently on table
    this.cardsOnTable = [];

    // briskula card, will be set when the game starts
    this.briskula = undefined;

    // set the current player to the first one in the queue
    this.players.reset();
  }

  // wait for their confirmation from the players that they got the message
  // after that, when everyone is ready, start the game.
  listenConnections() {
    this.nsp.use((socket, next) => {
      if (!socket.handshake.auth || !socket.handshake.auth.key) {
        logger.warn(`Unauthorized socket ${socket.id} trying to connect (no key)`);
        socket.disconnect();
        return;
      }

      const player = this.players.getAll().find(p => p.key === socket.handshake.auth.key);
      
      if (!player) {
        logger.warn(`Unauthorized socket ${socket.id} trying to connect (wrong key)`);
        socket.disconnect();
        return;
      }
      if (player.ready) {
        logger.warn(`${socket.id} is trying to connect as an already ready player`);
        socket.disconnect();
        return;
      }
      next();
    });

    this.nsp.on("connection", async (socket) => {
      // this can't be undefined since all checks have been performed in the func above
      const player = this.players.getAll().find(p => p.key === socket.handshake.auth.key);

      if (player.dcd) {
        await this.handleReconnPlayer(player, socket);
      } else {
        this.handleNewPlayer(player, socket);
      }
      
      // if everyone's ready, start the game
      if (this.players.getAll().every(p => p.ready) && !this.isStarted) {
        this.start();
      }
      // report if any of the players are disconnected
      if (this.players.getAll().some(p => p.dcd)) {
        this.broadcastDcdPlayers(undefined, player);
      }

      // VVV *LISTENERS VVV
      
      // chat listener
      // data is in format { message: string }
      socket.on("chat", (data) => {
        const player = this.players.getData(socket.id);
        logger.infoNoConsole(`[CHAT] ${player.name}: ${data.message}`)

        socket.broadcast.emit("chat", {
          holder: player.name,
          message: data.message
        });
      });

      // game update listener
      // data is in format { action: string, payload: { card: Card }}
      socket.on("gameUpdate", (data) => {
        // the only available action is playCard, drop everything else
        if (data.action !== "playCard") {
          logger.warn("Unrecognised gameUpdate action: ", data.action);
          return;
        }

        // if the answered socket isn't the one that should take turn, return
        if (this.turnsDisabled || this.players.current.socket.id !== socket.id) {
          logger.warn(`Unexpected player tried to take a turn (${socket.id} should be ${this.players.current.socket.id})`);
          return;
        }

        // check if the card is in the player's hand
        const player = this.players.getData(socket.id);
        const payloadCard = data.payload.card;

        const found = player.cards.find((card) => {
          return card.sign === payloadCard.sign && card.number === payloadCard.number;
        });

        // if it isn't, return
        if (!found) {
          logger.error(`Unexisting card ${payloadCard} in player's hand (${socket.id})`);
          return;
        }

        // if it's tresette, check whether the card is valid
        if (this.mode.game === "treseta" && this.cardsOnTable.length > 0 && payloadCard.sign !== this.cardsOnTable[0].sign) {
          const any = player.cards.find(c => c.sign === this.cardsOnTable[0].sign);
          if (any) {
            logger.error(`Player ${socket.id} tried to play`, payloadCard, "when he had", any, "(dictated by", this.cardsOnTable[0], ")");
            return;
          }
        }

        // now all checks should be done. If everything's ok, play the turn.
        this.playTurn(payloadCard);
      });

      // for reconnected players
      socket.on("fullUpdate", () => {
        const player = this.players.getAll().find(p => p.socket.id === socket.id);
        const enemies = this.players.getAll().filter(p => p.id !== player.id);
        socket.emit("fullUpdate", {
          hand: player.cards,
          enemies: enemies.map(p => { return { playerID: p.id, cardNumber: p.cards.length } }),
          deck: this.getDeckLength(),
          briskula: this.BRISKULA_CONST,
          roundNumber: this.roundNumber,
          tableCards: this.cardsOnTable,
          current: this.players.current.id,
        })
      });

      // for restart request
      socket.on("restart", (options) => {
        if (this.isStarted) {
          logger.warn("Attempted restart request while a game is in process");
          return;
        }
        const player = this.players.getAll().find(p => p.socket.id === socket.id);

        if (!player) {
          logger.error("An unknown player has requested a restart");
          return;
        }

        // if a player doesn't want to play anymore, abort everything
        if (!options.confirm) {
          this.nsp.emit("restart", {
            ready: this.rematchPlayers.length >= this.mode.maxPlayers,
            unavailable: true,
            playerNum: this.rematchPlayers.length,
            outOf: this.mode.maxPlayers,
          });
          this.close();
          return;
        }

        const n = this.rematchPlayers.filter(id => id !== player.id);

        // the length should not be equal if the player was already in the array
        if (n.length !== this.rematchPlayers.length) {
          logger.warn(`Player ${player.name} sent a reconnect req but sent one already`);
          return;
        }

        // mark the player restart-ready
        this.rematchPlayers.push(player.id);

        this.nsp.emit("restart", {
          ready: this.rematchPlayers.length >= this.mode.maxPlayers,
          unavailable: false,
          playerNum: this.rematchPlayers.length,
          outOf: this.mode.maxPlayers,
        });

        // if all players have connected
        if (this.rematchPlayers.length >= this.mode.maxPlayers) {
          logger.info(`Restarting game ${this.id}, mode ${this.mode.name} and players ${this.players.getAll().map(v => v.name)}`);
          this.setInitialValues();
          this.start();
          // TODO this.waitForPlayers();
        }
      })
    })
  }

  // PARAMETERS: player - typeof Player, socket - typeof socket
  handleNewPlayer(player, socket) {
    // update the player's socket and set its status to ready
    player.socket = socket;
    player.ready = true;

    // switch the dc listener from global socket to the nsp socket
    player.globalSocket.off("disconnect", player.listener);
    player.socket.on("disconnect", player.listener);
  }

  // PARAMETERS: player - typeof Player, socket - typeof socket
  async handleReconnPlayer(player, socket) {
    const conn = await this.requestConnection(this.id, player.key);

    // conn doesn't exist, abort
    if (!conn) {
      socket.disconnect();
      return;
    }
    logger.info(`Player reconnected: ${player.id} (${player.name})`);

    // the conn exists and the player is authenticated, proceed to update the
    // players object and synchronise the client with the state of the game
    conn.updateData({ game: this.id })
    player.globalSocket = conn.socket;
    player.conn = conn;
    player.socket = socket;
    player.ready = true;
    player.dcd = false;
    
    // setup a dc listener, but not on the global socket but the nsp one
    this.setupDCListener(player, true);

    // if there aren't any more dcd players, remove the game close timeout
    if (this.players.getAll().every(p => !p.dcd)) clearTimeout(this.dcTimeout);
    
    // tell everyone except the reconnected player who's dcd now
    this.broadcastDcdPlayers(player);

    const dcdPlayers = this.players.getAll().filter(p => p.dcd).map(p => p.id);

    // emit the data necessary to reconnect
    socket.emit("reconn", {
      success: true,
      mode: this.mode.name,
      name: player.name,
      playerID: player.id,
      startTime: this.startTime.getTime(),
      players: this.players.getAll().filter(p => p.id !== player.id).map(p => {
        return { id: p.id, name: p.name, isTeammate: player.team === p.team }
      }),
      dcd: {
        time: this.timeLastDcd,
        waitingFor: 30,
        allHere: dcdPlayers.length === 0,
        players: dcdPlayers,
      }
    });
  }

  // PARAMETERS: player - typeof Player, nonGlobal - boolean
  setupDCListener(player, nonGlobal) {
    const listener = () => {
      // if the game is not in process, clean up everything
      if (!this.isStarted) {
        this.nsp.emit("restart", {
          ready: this.rematchPlayers.length >= this.mode.maxPlayers,
          unavailable: true,
          playerNum: this.rematchPlayers.length,
          outOf: this.mode.maxPlayers,
        });
        this.close();
        return;
      }

      // if the game is in process
      player.dcd = true;
      player.ready = false;

      // mark time of the last player dcd
      this.timeLastDcd = Date.now();

      this.markPlayerDCd(this.id, player.key)

      // if all players don't join since the last player dc, end the game
      clearTimeout(this.dcTimeout);
      this.dcTimeout = setTimeout(() => {
        if (this.done) return;
        // broadcast the game end
        this.broadcastDcdPlayers(undefined, undefined, true);
        this.close();
      }, 30000);


      // if every player is DCd, close the game
      if (this.players.getAll().every(p => p.dcd)) {
        this.close();
        return;
      }

      this.broadcastDcdPlayers(player);
    };

    // bind a one-time disconnect listener.
    // if nonGlobal is set (which is usually for reconnected players), bind the
    // listener directly to the socket object rather than binding it to the
    // global socket and switching it moments later.
    if (nonGlobal) {
      player.socket.once("disconnect", listener);
    } else {
      player.globalSocket.once("disconnect", listener);
    }
    // bind the listener to the player object so it can be removed later
    player.listener = listener;
  }

  start() {
    // mark the game as started
    this.isStarted = true;

    // tell all players that the game is ready
    this.nsp.emit("gameStatus", { status: "ready" });

    // initial cards
    this.players.getAll().forEach(player => {
      const randomCards = [];

      // pick the max cards allowed
      for (let i = 0; i < this.mode.maxCards; i++) {
        const randomCard = this.pickRandomFromDeck();
        randomCards.push(randomCard);
      }

      // update the player object and send events to the players
      this.setCardsForPlayer(player, randomCards);
    });

    // if there are teams defined, synchronise the this.players array accordingly and randomise
    // the starting player, but if not, randomise the teams and the starting player
    if (this.TEAMCONFIG) {
      this.players.synchronise(this.TEAMCONFIG, this.mode.maxTeams);
      const shiftRandTimes = Math.floor(this.mode.maxPlayers * Math.random());
      for (let i = 0; i < shiftRandTimes; i++) {
        this.players.rotate();
      }
    } else {
      this.players.shuffle();
    }

    // pick the briskula card and broadcast it to the players
    // briskula is not set if the game is not briskula
    const briskula = this.mode.game === "briskula" && this.pickRandomFromDeck();

    // this.briskula is a card that is a part of the deck while this.BRISKULA_CONST
    // is a constant that won't change throughout the whole game
    this.briskula = briskula;
    this.BRISKULA_CONST = briskula;
    this.briskula && this.nsp.emit("briskulaUpdate", { briskula });

    // update the deck length
    this.nsp.emit("deckUpdate", { numberOfCards: this.getDeckLength() });

    // signal the start of a new round
    this.nsp.emit("newRound", { roundNumber: ++this.round });

    // send the information about whose turn is currently
    this.broadcastCurrentPlayer();
  }

  playTurn(payloadCard) {
    /*
      THIS FUNCTION ASSUMES THAT ALL CHECKS HAVE BEEN PERFORMED ALREADY
    */

    // this should be safe because it's already been checked
    const socket = this.players.current.socket;

    // push this card to the cardsOnTable array
    this.cardsOnTable.push(payloadCard);

    // delete it from the holder's hand
    this.deleteCardForSocket(socket, payloadCard);

    // tell everyone that the table cards have changed
    this.nsp.emit("tableCardUpdate", { cards: this.cardsOnTable });

    // check if this was the turn of the last player, if it wasn't, return
    if (!this.players.isLastPlayer()) {
      // switch current player and brodcast the update
      this.nextPlayer(true);
      return;
    }

    // for some modes, like 1v1 doubles, a player must play 2 times before
    // the round is over. If this.extraHand is defined, that means that such
    // mode is active. If it exists, and it's false, return.
    if (this.extraHand !== undefined && this.extraHand === false) {
      // switch current player and brodcast the update
      this.nextPlayer(true);
      // make sure that the next turn will pass
      this.extraHand = true;
      return;
    }
    // reset the var if it's defined
    if (this.extraHand !== undefined) this.extraHand = false;
    
    /*
      **********************************************
      THE CODE BELOW EXECUTES WHEN THE ROUND IS DONE
      **********************************************
    */

    // get index of the strongest card, that is the index of the
    // player in this.players.getAll() array who selected it
    const index = this.getStrongestCardIndex(this.cardsOnTable);

    // since the card index is also the team index, the index can be used to
    // track which team won the last round (might change while refractoring, beware)
    this.lastHandWonTeam = index;

    // broadcast win status to players and cards to the pile of the winners
    this.players.getAll().forEach((player, i) => {
      // if they are both even or odd, the player won the round, if
      // one is even and the other is odd, the player lost the round
      const roundWon = i % 2 === index % 2;

      if (roundWon) {
        player.cardStack.push(...this.cardsOnTable);
      }

      player.socket.emit("gameUpdate", {
        type: "roundWinner",
        payload: { roundWon }
      });
    });

    // broadcast that it's currently noone's turn
    this.turnsDisabled = true;
    this.broadcastCurrentPlayer();

    // after 3 seconds, start the new round
    setTimeout(() => {
      // deck is empty and briskula is taken and no player has any cards left, the game has ended
      if (this.getDeckLength() === 0 && !this.briskula) {
        if (this.players.getAll().every(v => v.cards.length === 0)) {
          this.gameEnd();
          return;
        }
      }

      // set the first player based on who had the strongest card
      // it has been taken a modulo of index because in some modes like double 1v1
      // it is possible that the third card is the strongest with only 2 players.
      // since card 1 has the same holder as card 3 (aka 3 % 2), it is safe to use it.
      const strongestPl = this.players.getAll()[index % this.mode.maxPlayers];
      this.players.setFirstPlayer(strongestPl.id);

      // check if briskula exists and store it in a var
      const briskulaExists = !!this.briskula;

      // draw cards for every player
      this.players.getAll().forEach((player) => {
        const pickedCards = []; 

        // fill it until there are enough cards in the hand but only until
        // there are cards in the deck and briskula exists
        while (player.cards.length + pickedCards.length < this.mode.maxCards && (this.getDeckLength() > 0 || this.briskula)) {
          const card = this.pickRandom();
          pickedCards.push(card);
        }

        // create a copy of player cards with the picked cards
        const cards = [...player.cards, ...pickedCards];

        // if the game mode is tresette, send the last drawn card to the users
        // this is a bit dirty but who cares
        const lastCard = this.mode.game === "treseta" && pickedCards.length > 0 ? pickedCards[pickedCards.length-1] : undefined;

        // update the player cards
        this.setCardsForPlayer(player, cards, lastCard);
      });

      // if the briskula has changed from true to false and it's 2v2 mode, send an
      // update of teammate's cards but only if the game is not tresette
      if (this.mode.game !== "treseta" && !this.briskula && !!this.briskula !== briskulaExists && this.mode.name.includes("2v2")) {
        // send each player their teammate's cards
        this.players.getAll().forEach((p, i) => {
          p.socket.emit("teammateUpdate", {
            cards: this.players.getAll()[(i + 2) % 4].cards
          });
        });
      }

      // enable turn taking again
      this.turnsDisabled = false;

      // switch the player to the next one and broadcast it
      this.nextPlayer(true);

      // reset cards on table
      this.cardsOnTable = [];

      // tell everyone that the table cards have changed
      this.nsp.emit("tableCardUpdate", { cards: this.cardsOnTable });

      // broadcast the deck update
      this.nsp.emit("deckUpdate", { numberOfCards: this.getDeckLength() });

      // broadcast the start of a new round
      this.nsp.emit("newRound", { roundNumber: ++this.round });
    }, 3000);
  }

  // pick random from either deck or briskula card, delete it and return it
  // RETURN VALUE: card - typeof card
  pickRandom() {
    // pick from deck if there's any cards left in it
    if (this.getDeckLength() > 0) return this.pickRandomFromDeck();

    // take the briskula card if it exists
    if (this.briskula !== undefined) {
      const returnCard = this.briskula;
      this.nsp.emit("briskulaUpdate", { briskula: undefined });
      this.briskula = undefined;
      return returnCard;
    }

    // there's an error if it had come to this point
    logger.error("No cards left in deck and briskula is undefined");
    return undefined;
  }

  // get random card from deck and delete it
  // RETURN VALUE: card - typeof Card
  pickRandomFromDeck() {
    // they are in format {sign, number}
    const card = this.getRandomFromDeck();
    this.deleteCardFromDeck(card);
    return card;
  }

  // get random card from deck, but don't delete it
  getRandomFromDeck() {
    // make it in format [sign, string[]]
    const arrayifiedDeck = Object.entries(this.cards.deck);

    // contains all cards that are available
    const availableCards = [];

    // pull all available cards in one array
    arrayifiedDeck.forEach(sign => {
      sign[1].forEach(number => availableCards.push({ sign: sign[0], number }));
    });

    // there are none cards left, return undefined
    if (availableCards.length === 0) {
      return undefined;
    }
    const randomIndex = Math.floor(availableCards.length * Math.random());
    const randomCard = availableCards[randomIndex];

    // randomCard isn't returned directly, separate object is created instead
    return {
      sign: randomCard.sign,
      number: randomCard.number
    }
  }

  // delete a card from deck
  deleteCardFromDeck(card) {
    // check if the card is undefined or parts of it
    if (!card || !card.sign || card.number === undefined) {
      logger.error("Cannot delete invalid card:", card);
      return;
    }

    // find index of the card
    const index = this.cards.deck[card.sign].findIndex(v => v === card.number);

    // if index is undefined, the card doesn't exist in the deck
    if (index === undefined) {
      logger.error(`Card ${card} can't be deleted since it doesn't exist in the deck`);
      return;
    }

    // delete the card
    this.cards.deck[card.sign].splice(index, 1);
  }

  getDeckLength() { // ( ͡° ͜ʖ ͡°)
    let count = 0;
    const arrayifiedDeck = Array.from(Object.entries(this.cards.deck));
    arrayifiedDeck.forEach(zog => {
      count += zog[1].length;
    });
    return count;
  }

  // lastCard is optional
  setCardsForPlayer(playerData, cards, newCard) {
    // check if the socket is undefined
    if (playerData.socket === undefined) {
      logger.error("Cannot update cards to an undefined socket");
      return;
    }
    
    // update the playerData object
    playerData.cards = cards;

    // emit the changes to the player
    playerData.socket.emit("handUpdate", { cards });
    
    // send cards in hand update to the other players
    this.nsp.as(playerData.socket).broadcast("enemyUpdate", {
      data: { playerID: playerData.id, cardNumber: cards.length, newCard }
    });
  }

  deleteCardForSocket(socket, card) {
    const newCards = [];

    const playerData = this.players.getData(socket.id);

    playerData.cards.forEach(c => {
      if (!(c.number === card.number && c.sign === card.sign)) {
        newCards.push(c);
      }
    });

    this.players.getData(socket.id).cards = newCards;

    // emit the changes to the player
    socket.emit("handUpdate", { cards: newCards });

    // send cards in hand update to the other players
    this.nsp.as(playerData.socket).broadcast("enemyUpdate", {
      data: { playerID: playerData.id, cardNumber: newCards.length }
    });
  }

  // set the current player to next and broadcasts to the connections the updated player
  // which can be controlled by the broadcast parameter.
  // PARAMETERS: broadcast - boolean
  nextPlayer(broadcast) {
    this.players.switchToNext();

    // if broadcast is falsy, don't broadcast the updated player
    if (!broadcast) return;

    this.broadcastCurrentPlayer(broadcast);
  }

  // broadcast the ID of the current player, if the turns are disabled, it
  // will send an empty string
  broadcastCurrentPlayer() {
    this.nsp.emit("gameUpdate", {
      type: "currentPlayerUpdate",
      payload: {
        current: this.turnsDisabled ? "" : this.players.current.id
      }
    });
  }

  // broadcast dcd players as dcd/reconn player
  // -- if asPlayer exists, if will broadcast as that player (he'll not recieve the message)
  // -- if toPlayer exists, the message will be broadcast only to that player
  // -- if neither exist, the message will be broadcast to all players
  broadcastDcdPlayers(asPlayer, toPlayer, isEnd) {
    const dcdPlayers = this.players.getAll().filter(p => p.dcd).map(p => p.id);

    const data = {
      gameEnd: !!isEnd,
      waitingFor: 30,
      time: this.timeLastDcd,
      allHere: dcdPlayers.length === 0,
      players: dcdPlayers
    }

    if (asPlayer) {
      this.nsp.as(asPlayer).broadcast("playerDC", data);
    } else if (toPlayer) {
      toPlayer.socket && toPlayer.socket.emit("playerDC", data);
    } else {
      this.nsp.emit("playerDC", data);
    }
  }

  getNumber(num) {
    switch (num) {
      case "3":
        return this.mode.game === "briskula" ? 13 : 16;

      case "2":
        return this.mode.game === "briskula" ? 2 : 15;

      case "F":
        return 10;

      case "K":
        return 11;

      case "R":
        return 12;

      case "A":
        return 14;
    }

    if (num >= 2 && num <= 7) return Number(num);

    logger.error("Unknown number:", num);
  }

  // PARAMETERS: cards - typeof Card[]
  getStrongestCardIndex(cards) {
    if (cards.length === 0) {
      logger.warn("Cannot get strongest card from empty array");
      return;
    }

    let strongest = 0; // typeof number
    let bestSign = this.BRISKULA_CONST.sign || cards[0].sign;

    cards.forEach((card, i) => {
      // skip the first card
      if (i === 0) return;

      // currently strongest card
      const cardStr = cards[strongest];

      // if the new one is stronger, update the currently strongest card index
      if (this.getStrongerCardIndex(cardStr, card, bestSign) === 1) strongest = i;
    })

    return strongest;
  }

  // PARAMETERS: bestSign - typeof string
  // RETURN VALUE: index - number
  getStrongerCardIndex(card1, card2, bestSign) {
    const sign1 = card1.sign;
    const sign2 = card2.sign;
    const num1 = this.getNumber(card1.number);
    const num2 = this.getNumber(card2.number);

    if (sign1 === bestSign && sign2 !== bestSign) {
      return 0;
    }
    if (sign1 !== bestSign && sign2 === bestSign) {
      return 1;
    }

    if (sign1 !== sign2) {
      return 0;
    }

    return num1 > num2 ? 0 : 1;
  }

  calculateScore(cards) {
    return this.mode.game === "briskula" ? this.scoreBriskula(cards) : this.scoreTreseta(cards);
  }

  // RETURN VALUE: an array with one item - score (typeof number)
  scoreBriskula(cards) {
    let score = 0;

    cards.forEach(card => {
      switch (card.number) {
        case "A":
          score += 11;
          break;
        case "3":
          score += 10;
          break;
        case "R":
          score += 4;
          break;
        case "K":
          score += 3;
          break;
        case "F":
          score += 2;
          break;
      }
    });

    return [score];
  }

  // RETURN VALUE: an array with two items - score (typeof number) and bele (typeof number)
  scoreTreseta(cards) {
    let score = 0;
    cards.forEach(card => {
      "32RKF".includes(card.number) && score++;
      if (card.number === "A") score += 3;
    });
    return [Math.floor(score / 3), score % 3];
  }

  // executed when the current game is finished
  gameEnd() {
    // take the first two players and calculate their scores. There are always
    // two teams so scores of the others players should be the same ones that
    // the first two players have
    const p1 = this.players.getAll()[0];
    const p2 = this.players.getAll()[1];
    
    // bele are not always defined, only when treseta is played
    let [score1, bele1] = this.calculateScore(p1.cardStack);
    let [score2, bele2] = this.calculateScore(p2.cardStack);

    // if the game is treseta, add one point to whoever won the last hand
    if (this.mode.game === "treseta") {
      this.lastHandWonTeam === 0 ? score1++ : score2++;
    }

    // cycle through the player array and send them points and win status
    this.players.getAll().forEach((player, i) => {
      let result = "lost";
      if (score1 == score2) result = "draw";
      if ((score1 > score2) === (i % 2 === 0)) result = "won";

      // update the player data
      result == "won" && player.wins++;
      result == "lost" && player.losses++;
      player.totalPoints += i % 2 === 0 ? score1 : score2;
      player.totalEnemyPoints += i % 2 === 0 ? score2 : score1;

      // send data to the player
      player.socket.emit("endgame", {
        result,
        points: i % 2 == 0 ? score1 : score2,
        bele: i % 2 == 0 ? bele1 : bele2,
        wins: player.wins,
        losses: player.losses,
        totalPoints: player.totalPoints,
        totalEnemyPoints: player.totalEnemyPoints,
      });
    });

    // signal that the game is not currently ongoing, but don't close everything yet
    // because a rematch is possible.
    this.isStarted = false;
  }

  // cleanup function which is executed when no more games will be played
  close() {
    this.done = true;

    logger.info("Stopped game", this.id);

    // clean up the nsp
    this.nsp.removeAllListeners();
    delete this.io._nsps["/game/" + this.id];

    // update the handle all listeners accordingly (if they exist) and update the player
    this.players.getAll().forEach(p => {
      p.globalSocket && p.globalSocket.off("disconnect", p.listener);
      p.socket && p.socket.off("disconnect", p.listener);
      p.socket && p.socket.disconnect();
      p.conn && p.conn.updateData({ game: undefined });
    });
    this.onEnd();
  }
}

export { Game }
