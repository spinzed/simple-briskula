import { Router } from "express";
import { getGame } from "../settings/utils.js";

const router = Router();

router.get("/", (req, res) => {
  const game = getGame(req.headers);
  const gameEng = game === "Treseta" ? "Tresette" : "Briscola"; 
  res.render("index.html", { game, gameEng });
});

/* OVO MORA OSTAT OBAVEZENO */
router.get("/bartol", (req, res) => {
  res.redirect("https://www.youtube.com/channel/UCumBvch-hIstSnjXwKltD8A?");
});

export default router;
