import { Server } from "socket.io";
import { createClient } from 'redis';
import redisAdapter from '@socket.io/redis-adapter';
import { GameManagment } from "../services/GameManagment.js";
import { MODES } from "../settings/constants.js";
import { SERVER_PORT, REDIS_HOST, REDIS_PORT } from "../settings/settings.js";
import Logger from "../settings/logger.js";
import { getGame } from "../settings/utils.js";
import process from 'node:process'; 

const logger = new Logger("SockCTL");

function getClientAddr(socket) {
  if (!socket || !socket.handshake || !socket.handshake.address) return "<unknown>";
  if (socket.handshake.headers && socket.handshake.headers["x-forwarded-for"]) {
    return socket.handshake.headers["x-forwarded-for"];
  }
  return socket.handshake.address;
}

function getUserAgent(socket) {
  if (!socket || !socket.handshake || !socket.handshake.headers) return "<unknown>";
  return socket.handshake.headers["user-agent"] || "<unknown>";
}

const createSocket = (server) => {

  // Initialise a socketio instance
  const io = new Server(server, {
    cors: {
      origin: "http://localhost:5173",
      methods: ["GET", "POST"],
      credentials: true
    }
  });

  // this enables use of sockets between multiple nodejs instances
  const pubClient = createClient({ host: REDIS_HOST, port: REDIS_PORT });
  const subClient = pubClient.duplicate();
  io.adapter(redisAdapter(pubClient, subClient));

  // GameManagment controls everything regarding connections, lobbies and games
  const Manager = new GameManagment(io);

  /* 
    ======================
    LOG SOCKET CONNECTIONS
    ======================
  */

  io.on("connection", socket => {
    const game = getGame(socket.handshake.headers);
    logger.info(`[${game}] Socket CON:`, socket.id, getClientAddr(socket), getUserAgent(socket));

    socket.on("disconnect", () => {
      logger.info(`[${game}] Socket DCD:`, socket.id, getClientAddr(socket), getUserAgent(socket));
    });
    // ping listener
    socket.on("ping", () => {
      socket.emit("pong")
    })
  });

  /* 
    =====================
    LOGIC VVVVVVVVVVVVVVV
    =====================
  */

  io.on("connection", socket => {
    const connection = Manager.openConnection(socket);

    socket.on("disconnect", () => {
      Manager.closeConnection(connection);
    });

    // THIS REGISTERS DIFFERENT SOCKET REQUESTS WHEN THEY ARE IN LOBBY
    /* PARAMERTERS: data: {
      type: queue | makeLobby | joinLobby
      payload: {
        name: string,
        modes: []string
      }
    }  */
    socket.on("gameStart", async (data) => {
      switch (data.type) {
      // HANDLE SOCKET'S REQUEST FOR QUEUE
        case "joinQueue":
          // if the name has been passed in, return
          if (!data.payload.name) {
            logger.warn("No name passed in");
            socket.emit("gameStart", { type: "joinQueue", payload: { success: false }});
            return;
          }

          // update name
          connection.updateData({ name: data.payload.name });
          var { modes } = data.payload;

          // if no modes have been passed, return
          if (modes === undefined || modes.length === 0) {
            logger.warn("No modes passed in");
            socket.emit("gameStart", { type: "joinQueue", payload: { success: false }});
            return;
          }

          // check if modes all modes are supported, if not return
          var unsupported = modes.find((modeName) => {
            return !MODES.find(m => m.name === modeName);
          });

          if (unsupported !== undefined) {
            logger.warn("Unsupported game mode", unsupported);
            socket.emit("gameStart", { type: "joinQueue", payload: { success: false }});
            return;
          } 

          // put the player in queue with desired mode
          Manager.queuePlayer(connection, modes);

          logger.info(`${socket.id} queued for ${modes} with the name "${data.payload.name}"`);

          socket.emit("gameStart", {
            type: "joinQueue",
            payload: { success: true }
          });

          Manager.attemptMatchAllModes();
          break;
      // HANDLE SOCKET'S REQUEST TO LEAVE THE QUEUE
      case "leaveQueue":
        var wasInQueue = Manager.leaveQueue(connection);

        socket.emit("gameStart", {
          type: "leaveQueue",
          payload: { success: true, wasInQueue }
        });
        break;
      // HANDLE SOCKET'S REQUEST FOR LOBBY CREATION
      case "makeLobby":
        // if the name has been passed in, return
        if (!data.payload.name) {
          logger.warn("No name passed in");
          socket.emit("gameStart", { type: "makeLobby", payload: { success: false }});
          return;
        }

        connection.updateData({ name: data.payload.name });
        var { mode } = data.payload;

        // if a mode hasn't been passed, return
        if (mode === undefined) {
          logger.warn("Mode not passed in");
          socket.emit("gameStart", { type: "makeLobby", payload: { success: false }});
          return;
        }

        // check if modes ais supported, if not return
        if (!MODES.find(m => m.name === mode)) {
          logger.warn("Unsupported game mode", mode);
          socket.emit("gameStart", { type: "makeLobby", payload: { success: false }});
          return;
        } 

        var lobby = await Manager.createLobby(mode);
        var reason = await Manager.joinLobby(lobby.id, connection);

        if (!reason) {
          logger.info(`${socket.id} made lobby ${lobby.id} of mode ${mode} w/ the name "${data.payload.name}"`);
        } else {
          Manager.closeLobby(lobby);
        }

        socket.emit("gameStart", {
          type: "makeLobby",
          payload: {
            success: !reason,
            lobbyID: !reason ? lobby.id : undefined
          }
        });
        break;
        
      // HANDLE SOCKET'S REQUEST TO JOIN LOBBY
      case "joinLobby":
        // if the name has been passed in, return
        if (!data.payload.name) {
          logger.warn("No name passed in");
          socket.emit("gameStart", { type: "joinLobby", payload: { success: false }});
          return;
        }

        connection.updateData({ name: data.payload.name });
        var { lobbyID } = data.payload;

        if (lobbyID === undefined) {
          logger.error("Invalid lobby ID");
          socket.emit("gameStart", { type: "joinLobby", payload: { success: false, lobbyError: true }});
          return;
        }

        // return value indicates if everything went Alright(TM) by Kendrick Lamar
        reason = await Manager.joinLobby(lobbyID, connection);

        if (!reason) {
          logger.info(`${socket.id} joined the lobby ${lobbyID} w/ the name "${data.payload.name}"`);
        }

        socket.emit("gameStart", {
          type: "joinLobby",
          payload: { success: !reason, reason }
        });
        break;
      
      // HANDLE SOCKET'S REQUEST TO START A LOBBY GAME
      case "startLobbyGame":
        var opts = data.payload.opts;
        if (!opts.random && !opts.teams) { // opts.team typeof string[] (id[])
          logger.warn("No teams with random off in lobby starting options")
          socket.emit("gameStart", { type: "startLobbyGame", payload: { success: !failReason, reason: "noteams" }});
        }
        var failReason = await Manager.startLobbyGameAs(connection, opts);
        socket.emit("gameStart", { type: "startLobbyGame", payload: { success: !failReason, reason: failReason }});
        break;

      case "reconnect":
        var { id, key } = data.payload;
        if (!id || !key) {
          logger.warn("Reconnect data missing:", id, key);
          socket.emit("gameStart", { type: "reconnect", payload: { success: false }});
          return;
        }
        // if the reconnect is possible, return true, if not then false
        var canReconn = await Manager.checkReconnect(socket.id, id, key);

        socket.emit("gameStart", { type: "reconnect", payload: { success: canReconn }});
        break;
      }
    });
  });

  return io;
}

export { createSocket };
