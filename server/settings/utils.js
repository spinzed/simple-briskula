import { deserialize, serialize } from "v8";

// return a copy of the from object with only the specified keys
function pick(from, keys) {
  const obj = {};
  for (const key of keys) {
    const val = from[key];
    if (typeof val === "function") continue;
    if (Object.prototype.hasOwnProperty.call(from, key)) {
      obj[key] = from[key];
    }
  }
  return obj;
}

// check whether are two object equal by comparing their keys
function isEqual(obj1, obj2) {
  for (const key of Object.keys(obj1)) {
    if (typeof val === "function") continue;
    if (!Object.prototype.hasOwnProperty.call(obj2, key)) return false;
  }
  for (const key of Object.keys(obj2)) {
    if (typeof val === "function") continue;
    if (!Object.prototype.hasOwnProperty.call(obj1, key)) return false;
  }
  return true;
}

// deep clone an object
function structuredClone (obj) {
  return deserialize(serialize(obj));
}

// get the game from headers
function getGame(headers) {
  if (!headers) return "<unknown>";
  const host = headers["x-forwarded-host"] || headers.host;
  const port = headers["x-forwarded-port"] || headers.host.split(":")[1] || "80";
  return host.includes("treseta") || port.includes("8080") ? "Treseta" : "Briskula";
}

// zip a matrix array n x m. Not very secure implementation
function zip(arr) {
  const result = [];
  for (let n = 0; n < arr[0].length; n++) {
    for (let m = 0; m < arr.length; m++) {
      result.push(arr[m][n]);
    }
  }
  return result;
}

export {
  pick,
  isEqual,
  structuredClone,
  getGame,
  zip
};
