// returns time in format %h:%m:%s
function time() {
  const date = new Date();
  return `${nDigits(date.getDate())}/${nDigits(date.getMonth()+1)}/${date.getFullYear()} ${nDigits(date.getHours(), 2)}:${nDigits(date.getMinutes(), 2)}:${nDigits(date.getSeconds(), 2)}`;	
}

// formats the number to string which always has n digits
// return value: number
function nDigits(num, n) {
  return num.toLocaleString("en-US", { minimumIntegerDigits: n });
}

// make the string fixed length.
// return value: string
function fixedLength(string, len) {
  return string.substring(0, len).padEnd(len);
}

export {
  time,
  fixedLength
};
