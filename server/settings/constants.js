export const SIGNS = { kupe: "kupe", spade: "spade", bati: "bati", dinari: "dinari" };
export const CARDS = {
    // kupe: ["A", "2", "3", "4", "5", "6", "7", "F"],
  kupe: ["A", "2", "3", "4", "5", "6", "7", "F", "K", "R"],
  spade: ["A", "2", "3", "4", "5", "6", "7", "F", "K", "R"],
  bati: ["A", "2", "3", "4", "5", "6", "7", "F", "K", "R"],
  dinari: ["A", "2", "3", "4", "5", "6", "7", "F", "K", "R"],
}
export const MODES = [
  {
    name: "brisk-1v1",
    game: "briskula",
    maxTeams: 2,
    maxPlayers: 2,
    maxCards: 3,
  },
  {
    name: "brisk-1v1-dupla",
    game: "briskula",
    maxTeams: 2,
    maxPlayers: 2,
    maxCards: 4,
  },
  {
    name: "brisk-2v2",
    game: "briskula",
    maxTeams: 2,
    maxPlayers: 4,
    maxCards: 3,
  },
  {
    name: "tres-1v1",
    game: "treseta",
    maxTeams: 2,
    maxPlayers: 2,
    maxCards: 10,
  },
  {
    name: "tres-2v2",
    game: "treseta",
    maxTeams: 2,
    maxPlayers: 4,
    maxCards: 10,
  }
];
