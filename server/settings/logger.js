import { time, fixedLength } from "./format.js";
import { createWriteStream } from "fs";

let stream; // typeof fs write stream

class Logger {
  // PARAMETERS: service - string
  constructor(service) {
    this.service = service || "";

    // open the file stream only if one hasn't been opened already
    if (!stream) {
      stream = createWriteStream("./log.txt", { flags: "a" })
    }
  }
  
  // PARAMETERS: message - string
  info(...message) {
    const msg = this.__formatMsg("INF", message);
    this.__info(msg);
    stream.write(msg + "\n");
  }

  // PARAMETERS: message - string
  infoNoConsole(...message) {
    const msg = this.__formatMsg("INF", message);
    stream.write(msg + "\n");
  }

  __info(msg) {
    console.info(msg);
  }

  // PARAMETERS: message - string
  warn(...message) {
    const msg = this.__formatMsg("WRN", message);
    console.warn(msg);
    stream.write(msg + "\n");
  }

  // PARAMETERS: message - string
  error(...message) {
    const msg = this.__formatMsg("ERR", message);
    console.error(msg);
    stream.write(msg + "\n");
  }

  // PARAMETERS: type - string, message - string
  // RETURN VALUE: string
  __formatMsg(type, message) {
    return `[${fixedLength(this.service, 7)} ${type}] ${time()} ${message.join(" ")}`;
  }
}

export default Logger;
