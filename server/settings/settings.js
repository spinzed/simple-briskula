import process from 'node:process'; 

export const SERVER_PORT = process.argv[2] || 3000;
export const REDIS_HOST = process.env.NODE_ENV === "production" ? "redis" : "localhost";
export const REDIS_PORT = 6379;
