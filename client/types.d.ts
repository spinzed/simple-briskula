declare module "*.module.css";
declare module "*.webp";

declare namespace Game {
  type CardSign = "kupe" | "spade" | "bati" | "dinari" | "back";
  type CardNumber = "A" | "2" | "3" | "4" | "5" | "6" | "7" | "F" | "K" | "R";
  
  interface Card {
    sign: CardSign;
    number?: CardNumber;
    id?: number; // used to distinguish different cards if they have the "back" sign
  }
}

type S = import("socket.io-client").Socket;
type D = import("@socket.io/component-emitter").DefaultEventsMap;

declare type Game = "briskula" | "treseta";

declare interface Mode {
  name: string;
  showName: string;
  game: Game;
  maxPlayers: number;
  maxTeams: number;
}

declare namespace Data {
  interface General {
    mode: Mode;
    uuid: string;
    playerID: string;
    key: string;
    name: string;
    startTime: number;
    players: Player[];
    dcd?: {
      time: number;
      waitingFor: number;
      allHere: boolean;
      players: string[];
    }
    nsp?: S;
  }

  interface Player {
    id: string;
    name: string;
    isTeammate: boolean;
  }

  interface GameEnd {
    result: "won" | "lost" | "draw";
    points: number;
    bele?: number;
    wins: number;
    losses: number;
    totalPoints: number;
    totalEnemyPoints: number;
    // the socket is not yet disconnected when the game ends, the game may
    // be restarted so the nsp must be carried over to the game end screen
    nsp: S;
  }
}
