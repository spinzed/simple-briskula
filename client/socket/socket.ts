import { createContext } from "react";
import io, { Socket } from "socket.io-client";
import { DefaultEventsMap } from "@socket.io/component-emitter";

interface RefreshableSocket extends Socket<DefaultEventsMap, DefaultEventsMap> {
  refresh: () => void;
}

const mode = import.meta.env.MODE;
const domain = mode === "production" ? "" : "localhost:3000";

const createSocket = (): RefreshableSocket => {
  let socket = io(domain) as RefreshableSocket;
  // this isn't really used anymore, but good to have nonetheless
  socket.refresh = () => {
    socket.disconnect();
    socket = createSocket();
  };

  socket.on("connect", () => {
    console.info("made socket connection with id", socket.id);
  });

  return socket;
};

export const makeNsp = (room: string, key: string | undefined): Socket<DefaultEventsMap, DefaultEventsMap> => {
  const path = domain + "/game/" + room;
  const nsp = io(path, { auth: { key }});
  return nsp;
}

export const SOCK = createSocket();

export const SocketCtx = createContext(SOCK);
