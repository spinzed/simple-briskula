import PopupOverlay from "../PopupOverlay/PopupOverlay";
import styles from "./PopupMenu.module.css";

interface PopupMenuProps {
  in: boolean;
  children: React.ReactNode;
  className?: string;
  onExit?: () => void;
}

const PopupMenu = (props: PopupMenuProps): JSX.Element => {
  const listener = (e: KeyboardEvent) => {
    if (e.key === "Escape" && props.in && props.onExit) props.onExit();
  }
  return (
    <PopupOverlay in={props.in} opacity={0.25} className={`${styles.PopupMenu} ${props.className || ""}`} onKeyDown={listener} onClickOutside={props.onExit}>
      {props.children}
    </PopupOverlay>
  );
}

export default PopupMenu;
