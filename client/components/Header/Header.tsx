import { useState, useEffect, useContext } from "react";
import styles from "./Header.module.css";
import { ping } from "../../utils/socket";
import { SocketCtx } from "../../socket/socket";

const Header = () => {
  const [data, setData] = useState<number | undefined | null>(null);
  const socket = useContext(SocketCtx);

  useEffect(() => {
    setTimeout(() => {
      ping(socket, setData);
    }, 750);
  }, []);

  return (
    <div className={styles.Header}>
      <div className={styles.balota} style={{
        backgroundColor: typeof data === "number" ? "green" : data === null ? "#4b4b4b" : "red"
      }}></div>
      <p>
        {typeof data === "number" && `Server online (${data}ms)`}
        {data === null && "Pinging..."}
        {data === undefined && "No connection"}
      </p>
    </div>
  )
}

export default Header;
