import styles from "./SlickInputField.module.css"

interface SlickInputFieldProps extends
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> { }

function SlickInputField(props: SlickInputFieldProps) {
  return (
    <div className={styles.SlickInputField} style={props.style}>
      <input
        type="text"
        className={styles.input}
        maxLength={36}
        spellCheck={false}
        {...props}
      />
    </div>
  )
}

export default SlickInputField;
