import { useState, useEffect } from "react";
import styles from "./CookieBanner.module.css";
import Button from "../Button/Button";
import { isPhone } from "../../utils/client";
import Opacity from "../Opacity/Opacity";

interface CookieBannerProps {
  show: boolean;
  onCookieConsent: (result: boolean) => void;
}

const CookieBanner = (props: CookieBannerProps) => {
  const [firstRender, setFirstRender] = useState(true);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setFirstRender(false);
    }, 0);
    return () => clearTimeout(timeout);
  }, []);

  return (
    <Opacity
      in={!firstRender && props.show}
      timeout={{ enter: 2000, exit: 500 }}
      unmountOnExit
    >
      <div className={`${styles.CookieBanner} ${isPhone() ? styles.CookieBannerPhone : ""}`}>
        <div className={styles.message}>
          Can we use cookies to store your name so you don't have to reenter it every time you visit this page?
        </div>
        <Button size={"small"} onClick={() => props.onCookieConsent(true)}>Yee</Button>
        <Button size={"small"} onClick={() => props.onCookieConsent(false)}>Nah</Button>
      </div>
    </Opacity>
  )
}

export default CookieBanner;
