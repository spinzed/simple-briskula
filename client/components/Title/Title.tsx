import { capitalFirst } from "../../utils/format";
import { game } from "../../utils/game";
import styles from "./Title.module.css"

interface TitleProps {
  fontSize?: string;
  noAnim?: boolean;
}

const Title = (props: TitleProps) => {
  let number: string = "";
  let string: string = "";

  let smallStyles: React.CSSProperties = {};

  if (props.fontSize !== undefined) {
    Array.from(props.fontSize).forEach((v) => {
      isNaN(Number(v)) ? string += v : number += v;
    })
  
    const trueNum = Number(number);

    smallStyles = {
      marginLeft: String(trueNum * 0.9) + string,
      marginTop: String(trueNum / 5) + string,
      fontSize: String(trueNum / 5) + string,
    }
  }

  return (
    <div className={`${styles.Title} ${props.noAnim ? styles.noanim : ""}`}>
      <div style={ smallStyles } className={styles.aboveText}>Small & Simple...</div>
      <h1 style={{ fontSize: props.fontSize }} className={styles.titleMain}>{capitalFirst(game())}</h1>
    </div>
  )
};

export default Title;
