import { useState } from "react";
import Button from "../Button/Button";
import { CSSTransition } from "react-transition-group";
import { setShowFAQ, showFAQ } from "../../utils/cookie";
import styles from "./FAQ.module.css";
import FAQTransitions from "./FAQTransitions.module.css";
import { game, standardGameName } from "../../utils/game";

const FAQ = () => {
  const [visible, setVisible] = useState(showFAQ());

  return (
    <div className={`${styles.FAQ} nomobile`}>
      <span>
        <Button
          size="small"
          className={styles.butt}
          onClick={() => { setVisible(!visible); setShowFAQ(!visible) }}
        >
          {visible ? "Collapse" : "Show"}
        </Button>
        <h2>FAQ</h2>
      </span>
      <CSSTransition
        in={visible}
        timeout={500}
        classNames={FAQTransitions}
        unmountOnExit
      >
        <div>
          <h3>What is this?</h3>
          <p>Traditional Italian card game {standardGameName()} recreated in browser.</p>
          <h3>Well, how do I play?</h3>
          <a href={`https://en.wikipedia.org/wiki/${standardGameName()}`} target="_blank" rel="noreferrer">https://en.wikipedia.org/wiki/{standardGameName()}</a>
          <p>For the best experience, learn it from your Italian friend.</p>
          <h3>How do I join a game?</h3>
          <p>Click "Find Game" for quick matchmaking (if possible). To make a personal lobby, click on "Create Lobby"
             and your friend can join it by clicking "Join Lobby" and pasting the lobby ID.</p>
          <p><strong>There aren't many playes playing this game at a given time, so I'd advise you to call a friend to play together with you.</strong></p>
          <h3>What modes are available?</h3>
          <p>Plain 1v1{game() === "briskula" && ", double 1v1"} and 2v2.</p>
          <h3>Additional Notes?</h3>
          <p>This implementation uses the Triestine card set (others like Napoletane or Siciliane are currently unsupported).</p>
        </div>
      </CSSTransition>
    </div>
  )
}

export default FAQ;
