import styles from "./InputField.module.css";

interface InputFieldProps extends
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> { }

const InputField = (props: InputFieldProps) => {
  return (
    <div className={styles.InputField}>
      <label htmlFor="username" className={styles.text}>What is the warrior's name?</label>
      <input
        type="text"
        id="username"
        className={styles.username}
        maxLength={16}
        spellCheck={false}
        {...props}
      />
    </div>
  )
}

export default InputField;
