import React, { CSSProperties } from "react";
import styles from "./Button.module.css";

interface ButtonProps {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  children: string;
  size?: string;
  className?: string;
  style?: CSSProperties;
}

const Button = (props: ButtonProps) => {
  let cn: string;

  if (props.size === "small") {
    cn = styles.small;
  } else {
    cn = styles.medium;
    if (props.children.length > 12) {
      cn = cn + " " + styles.smalltext;
    }
  }

  return (
    <div className={`${styles.Button} ${cn} ${props.className || ""}`} style={props.style}>
      <button
        className={styles.actualButton}
        disabled={props.disabled}
        onClick={props.onClick}
      >
        {props.children}
      </button>
    </div>
  )
};

export default Button;
