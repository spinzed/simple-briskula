import styles from "./PlayerSlot.module.css";

interface IPlayerSlotProps {
  slotId: string;
  message: string;
  onClick: () => any;
}

const PlayerSlot: (props: IPlayerSlotProps) => JSX.Element = (props) => (
  <div id={props.slotId} className={`${styles.PlayerSlot} ${styles[props.slotId]}`}
    key={props.slotId} onClick={props.onClick}>
    <span>Player {props.slotId.split("player")[1]}</span>
    <div id={props.slotId + "Status"} className={styles.status}>
      {props.message}
    </div>
  </div>
);

export default PlayerSlot;
