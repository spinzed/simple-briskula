import { useState, useRef, useEffect } from "react";
import { secondsSince } from "../../utils/format";
import styles from "./Timer.module.css";

interface TimerProps {
  startTime: number | undefined; // unix timestamp
  duration: number; // seconds
  radius?: number;
  stroke?: number;
}

const Timer = (props: TimerProps): JSX.Element => {
  const [timeLeft, setTimeLeft] = useState(props.duration);
  const circ = useRef<SVGCircleElement>(null);
  const int = useRef<number>();
  const restart = useRef(props.startTime);

  // the purpose of restart ref is to signal to the useEffect hook
  // to update only when props.startTime is changes, but not to undefined
  if (props.startTime !== undefined) restart.current = props.startTime;

  // fire only when startTime timestamp changes and it is not undefined or
  // when props.duration changes
  useEffect(() => {
    // restart the anim
    restartAnim();

    // reset the duration
    const start = Date.now();
    setTimeLeft(props.duration);

    // every second update the duration
    int.current = window.setInterval(() => {
      const secsPassed = secondsSince(start);
      const secsLeft = props.duration - secsPassed;
      setTimeLeft(secsLeft);
      if (secsLeft <= 0) clearInterval(int.current);
    }, 1000);

    // cleanup
    return () => clearInterval(int.current);
  }, [restart.current, props.duration]);

  const restartAnim = () => {
    const elm = circ.current;
    elm?.classList?.remove(styles.anim);
    void elm?.clientWidth;
    elm?.classList?.add(styles.anim);
  };

  const stroke = props.stroke || 5;
  const radius = props.radius || 40;
  const fullRadius = radius + stroke;
  const fullDiam = 2 * fullRadius;
  const circumference = 2 * radius * Math.PI;

  const stSvg = {
    width: fullDiam,
    height: fullDiam,
  };

  const stCirc = {
    strokeWidth: stroke,
    animationDuration: `${props.duration || 10}s`,
    "--circuference": circumference,
  }

  return (
    <div className={styles.Timer}>
      <div className={styles.number}>{timeLeft}</div>
      <svg style={stSvg}>
        <circle ref={circ} className={styles.anim} style={stCirc} r={radius} cx={fullRadius} cy={fullRadius}></circle>
      </svg>
    </div>
  )
}

export default Timer;
