import { useState, useRef, useEffect } from "react";
import styles from "./GameStats.module.css";
import { CSSTransition } from "react-transition-group";
import { friendlies, opponents } from "../../utils/format";
import Title from "../../components/Title/Title";
import { theme } from "../../utils/game";

interface GameStatsProps {
  data?: Data.General;
  roundWon: boolean | undefined;
  delay: number;
  isMyTurn: boolean;
}

const GameStats = (props: GameStatsProps) => {
  const timeStarted = useRef(props.data?.startTime || Date.now());
  const [timeElapsed, setTimeElapsed] = useState(0); // in seconds

  useEffect(() => {
    let stopwatch: number;
    const timeout = setTimeout(() => {
      stopwatch = window.setInterval(() => {
        const timeNow = Date.now();
        const milliseconds = timeNow - timeStarted.current;
        const seconds = Math.round(milliseconds / 1000);
        setTimeElapsed(seconds);
      }, 0); // ok how the hell is this working
    }, 1000);
    return () => {
      clearTimeout(timeout);
      clearInterval(stopwatch)
    };
  }, []);

  const state = props.isMyTurn ? styles.myTurn : styles.notMyTurn;
  const parsedMinutes = Math.floor(timeElapsed / 60);
  const parsedSeconds = timeElapsed % 60;

  const st = {
    transitionDelay: props.delay + "ms",
    "--lighter": theme.vibrant.rgb,
    "--darker": theme.darkBackground.rgb
  };

  return (
    <CSSTransition
      in={true}
      timeout={1000 + props.delay}
      appear
      mountOnEnter
      classNames={{
        appear: styles.allAppear,
        appearActive: styles.allAppearActive
      }}
    >
      <div className={styles.GameStats} style={st}>
        <div className={styles.margin}>
          <Title fontSize="4rem" noAnim />
          <div className={styles.mode}>{props.data ? props.data.mode.showName : "Loading..."}</div>
          <div className={styles.names}>
            {props.data ? `${friendlies(props.data).join(" & ")} vs ${opponents(props.data).join(" & ")}` : ""}
          </div>
          <div className={styles.time}>
            Elapsed: {parsedMinutes}:{String(parsedSeconds).length < 2 ? 0 : ""}{parsedSeconds}
          </div>
        </div>
        <CSSTransition
          in={props.roundWon !== undefined}
          timeout={500}
          mountOnEnter
          unmountOnExit
          classNames={{
            enter: styles.winEnter,
            enterActive: styles.winEnterActive,
            exitActive: styles.winExitActive
          }}
        >
          <div className={styles.winStatus}>
              {props.roundWon ? "Round Won" : "Round Lost"}
          </div>
        </CSSTransition>
        <div className={`${styles.identifier} ${state}`}>
          {props.isMyTurn ? "Your Turn" : "Waiting..."}
        </div>
      </div>
    </CSSTransition>
  );
}

export default GameStats;
