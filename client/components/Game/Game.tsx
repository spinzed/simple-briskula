import { useState, useRef, useEffect } from "react";
import styles from "./Game.module.css";
import Hand from "../Hand/Hand";
import Opponent from "../Opponent/Opponent";
import Table from "../Table/Table";
import GameStats from "../GameStats/GameStats";
import PopupOverlay from "../PopupOverlay/PopupOverlay";
import Button from "../Button/Button";
import Timer from "../Timer/Timer";
import { makeNsp } from "../../socket/socket";
import { resetKeyAndGameID } from "../../utils/cookie";
import { secondsSince, tresetaSort } from "../../utils/format";
import { game } from "../../utils/game";
import { SIGN_SORT } from "../../game/constants";

interface GameProps {
  data?: Data.General;
  transitionStartDelay: number;
  onGameEnd: (data: Data.GameEnd) => void;
  onMainMenu: () => void;
}

interface DCDPlayers {
  gameEnd: boolean;
  time: number;
  waitingFor: number;
  allHere: boolean;
  players: string[];
}

const Game = (props: GameProps): JSX.Element => {
  // ill rewrite this w/ reducers maybe
  const [cardsInHand, setCardsInHand] = useState<Game.Card[]>(() => []);
  const [cardsInDeck, setCardsInDeck] = useState<number>(0);
  // set initial player card numbers so that the order of players is preserved
  const [opponentCards, setOpponentCards] = useState<{ [player: string]: { number: number, lastCard?: Game.Card } }>(() => {
    const obj: { [key: string]: { number: number, lastCard?: Game.Card } } = {};
    props.data?.players.forEach(p => obj[p.id] = { number: 0 });
    return obj;
  });
  // ref has to be used to read state in a useEffect hook that doesn't update
  // when opponentCard changes
  const opponentRef = useRef(opponentCards);
  const [isGameStarted, setIsGameStarted] = useState<boolean>(false);
  const [briskula, setBriskula] = useState<Game.Card>();
  const [currentPlayerID, setCurrentPlayerID] = useState<string>("");
  const [tableCards, setTableCards] = useState<Game.Card[]>([]);
  const [teammateCards, setTeammateCards] = useState<Game.Card[]>();
  const [, setRoundNumber] = useState<number>(0);
  const [lastRoundWon, setLastRoundWon] = useState<boolean | undefined>(undefined);
  const [dcdPlayers, setDcdPlayers] = useState<DCDPlayers>();
  const [serverError, setServerError] = useState(false);

  const initDelay = props.transitionStartDelay;

  const isMyTurn = currentPlayerID === props.data?.playerID;
  const opponentNum = props.data ? props.data?.players.length : 0;

  // update data on reconnect or game restart and add event listener for full update
  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      if (e.ctrlKey && e.shiftKey && e.code === "KeyK") {
        props.data?.nsp?.emit("fullUpdate");
      }
    }
    document.addEventListener("keydown", handler);
    if (props.data?.dcd && !props.data?.dcd?.allHere) {
      const waitingFor = props.data.dcd.waitingFor - secondsSince(props.data.dcd.time);
      setDcdPlayers({ gameEnd: false, ...props.data.dcd, waitingFor });
    }
    if (props.data?.nsp) {
      // if the game has ended while the transition was happening,
      // don't do anything further and tell the player.
      if (props.data.nsp.disconnected) {
        setServerError(true);
        return;
      }
      props.data.nsp.emit("fullUpdate");
      // since the game is started, there is no need for synchronization
      // from the server, just start the thing there's a small delay because
      // if it's instant, some stuff will load ahead of time (deck cards)
      setTimeout(() => {
        setIsGameStarted(true);
      }, 100);
    }
    return () => document.removeEventListener("keydown", handler);
  }, []);

  // if all players have reconnected on dc, resume the game after 3 secs
  useEffect(() => {
    if (dcdPlayers?.allHere) {
      const t = setTimeout(() => {
        setDcdPlayers(undefined);
      }, 3000);
      return () => clearTimeout(t);
    }
  }, [dcdPlayers?.allHere]);

  useEffect(() => {
    // uuid must exist
    if (!props.data?.uuid) {
      console.error("invalid uuid")
      return;
    }
    console.info("[Game] UUID:", props.data.uuid);

    // if the nsp doesn't exist, create it (should happen only when a new game has started)
    if (!props.data.nsp) props.data.nsp = makeNsp(props.data.uuid, props.data.key);
    const nsp = props.data.nsp;

    nsp.on("fullUpdate", (response) => {
      console.info("[Game] Full update")
      // hand
      setCardsInHand(response.hand);
      // enemy cards
      response.enemies.forEach((enemy: { playerID: string; cardNumber: number; }) => {
        const op = { ...opponentRef.current };
        op[enemy.playerID] = { number: enemy.cardNumber };
        opponentRef.current = op;
        setOpponentCards(op);
      });
      // deck cards
      setCardsInDeck(response.deck);
      // briskula
      setBriskula(response.briskula);
      // round number
      setRoundNumber(response.roundNumber);
      // table cards
      setTableCards(response.tableCards);
      // current player
      setCurrentPlayerID(response.current);
    });
    nsp.on("gameStatus", (response: { status: string; }) => {
      if (response.status === "ready") {
        setIsGameStarted(true);
      } else {
        console.error("Unexpected game status");
      }
    });
    nsp.on("handUpdate", (response: { cards: Game.Card[] }) => {
      console.info("[Game] hand update");
      setCardsInHand(game() === "treseta" ? tresetaSort(response.cards, SIGN_SORT) : response.cards);
    });
    nsp.on("teammateUpdate", (response: { cards: Game.Card[] }) => {
      console.info(": teammate card update");
      setTeammateCards(response.cards);
    });
    nsp.on("enemyUpdate", (response: { data: { playerID: string, cardNumber: number, newCard?: Game.Card } }) => {
      console.info("[Game] enemy cards update");
      const op = { ...opponentRef.current };
      op[response.data.playerID] = { number: response.data.cardNumber, lastCard: response.data.newCard};
      opponentRef.current = op;
      setOpponentCards(op);
    });
    nsp.on("deckUpdate", (response: { numberOfCards: number }) => {
      console.info("[Game] deck update", response.numberOfCards);
      setCardsInDeck(response.numberOfCards);
    });
    nsp.on("briskulaUpdate", (response: { briskula: Game.Card }) => {
      console.info("[Game] briskula set");
      setBriskula(response.briskula);
    });
    nsp.on("newRound", (response: { roundNumber: number }) => {
      console.info("[Game] New Round");
      setRoundNumber(response.roundNumber);
      setLastRoundWon(undefined);
      setTableCards([]);
    });
    nsp.on("tableCardUpdate", (response: { cards: Game.Card[] }) => {
      setTableCards(response.cards);
    });
    nsp.on("playerDC", (response: DCDPlayers) => {
      // if the server says that the game has ended, just go to the main screen
      if (response.gameEnd) {
        props.onMainMenu();
        return;
      }
      setDcdPlayers(response);
    });
    nsp.on("gameUpdate", (data) => {
      if (data.type === "currentPlayerUpdate") {
        setCurrentPlayerID(data.payload.current);
      } else if (data.type === "roundWinner") {
        setLastRoundWon(data.payload.roundWon);
      }
    });
    nsp.on("endgame", (data: Data.GameEnd) => {
      if (!nsp) {
        console.error("Nsp not defined");
        return;
      }
      props.onGameEnd({...data, nsp});
    });

    return () => { nsp && nsp.off() };
  }, []);

  // on main menu handler
  const onMainMenu = () => {
    resetKeyAndGameID();
    props.onMainMenu();
  }

  // return dcd players in format pl1, pl2 & pl3
  const formatDcdPlayers = () => {
    const pl = dcdPlayers?.players.map((id) => props.data?.players.find(p => p.id === id)?.name);
    if (!pl || pl.length === 0) return "";
    if (pl.length === 1) return pl;
    const last = pl.pop();
    return pl.join(", ") + " & " + last;
  }

  return (
    <div className={styles.Game}>
      {!isGameStarted && (
        <div style={{ color: "white", fontSize: "2em" }}>Loading...</div>
      )}
      {isGameStarted && (
        <>
          <GameStats delay={initDelay + 2000} data={props.data} isMyTurn={isMyTurn} roundWon={lastRoundWon} />

          <div className={styles.opponents} style={opponentNum > 1 ? { right: "5%" } : {}}>
            {props.data?.players.map(p => (
              <Opponent
                key={p.id}
                delay={initDelay + 1500}
                current={currentPlayerID === p.id}
                player={p}
                cardNumber={opponentCards[p.id].number}
                lastCard={opponentCards[p.id].lastCard} />
            ))}
          </div>

          <Table
            delay={initDelay + 500}
            numberOfCards={cardsInDeck}
            briskula={briskula}
            tableCards={tableCards}
            lastWon={lastRoundWon || false}
          />
          <Hand
            delay={initDelay + 800}
            cards={teammateCards ? teammateCards : cardsInHand}
            isMyTurn={isMyTurn}
            nsp={props.data?.nsp}
            viewingTeammate={!!teammateCards}
            onSwitchCards={() => setTeammateCards(undefined)}
            name={props.data?.name}
            masterSign={game() === "treseta" ? tableCards[0]?.sign : undefined}
          />
        </>
      )}
      <PopupOverlay in={!!dcdPlayers && !serverError}>
        <h1 className={styles.title}>{dcdPlayers?.allHere ? "All's Good!" : "Oops!" }</h1>
        {dcdPlayers?.allHere ? (
          <div>All players have reconnected!</div>
        ) : dcdPlayers?.players.length === 1 ? (
          <p>The player with the name <span className={styles.n}>{formatDcdPlayers()}</span> has disconnected.</p>
        ) : (
          <p>Players <span className={styles.n}>{formatDcdPlayers()}</span> have disconnected.</p>
        )}
        {dcdPlayers?.allHere ? (
          <p>Resuming in:</p>
        ): (
          <p>Waiting for all players to reconnect.</p>
        )}
        <Timer startTime={dcdPlayers?.time} duration={!dcdPlayers || dcdPlayers.allHere ? 3 : dcdPlayers.waitingFor} />
        {!dcdPlayers?.allHere &&
          <Button className={styles.b} onClick={onMainMenu}>Return to Lobby</Button>
        }
      </PopupOverlay>
      <PopupOverlay in={serverError}>
        <h1>Oops!</h1>
        <p>Some players haven't reconnected in time so the game was terminated.</p>
        <Button className={styles.b} onClick={onMainMenu}>Return to Lobby</Button>
      </PopupOverlay>
    </div>
  );
};

export default Game;
