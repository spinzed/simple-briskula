import { useState, useRef, useEffect } from "react";
import styles from "./PhoneCheck.module.css";
import PopupOverlay from "../PopupOverlay/PopupOverlay";
import Button from "../Button/Button";
import { isFullscreen, isPWA, orientation } from "../../utils/client";

// This component will only be rendered if isMobile() is equal to true
const MobileCheck = (): JSX.Element => {
  const [isFull, setIsFull] = useState(isFullscreen());
  const isFullLast = useRef(isFullscreen());
  const [isLandscape, setIsLandscape] = useState(orientation() === "landscape");
  const fullEnabled = document.fullscreenEnabled;

  useEffect(() => {
    const listener = () => {
      const isFullNow = isFullscreen();
      setIsFull(isFullscreen());
      if (!isFullLast.current && isFullNow) {
        screen.orientation.lock("landscape"); // FIXME: gets called multiple times on fs
      }
      setIsLandscape(orientation() === "landscape")
      isFullLast.current = isFullNow;
    };
    window.addEventListener("resize", listener);
    return () => window.removeEventListener("resize", listener);
  }, []);

  const wait = "⌛ Waiting..."
  const done = "✅ Done!";
  const unav = "🤷 Unavailable..."

  return (
    // orientation isn't checked because it induces some bugs, also if it's fullscreen, it must be landscape
    <PopupOverlay in={fullEnabled && !isPWA() && !isFull} opacity={0.92} className={styles.PhoneCheck}>
      <h1>Hey!</h1>
      <p>To play this game, you must do the following:</p>
      <div>
        <p className={styles.yellow}>1) </p>
        <p>Enter fullscreen - <span className={styles.yellow}>{!fullEnabled ? unav : isFull ? done : wait}</span></p>
      </div>
      <div>
        <p className={styles.yellow}>2) </p>
        <p>Change the device orientation to landscape - <span className={styles.yellow}>{isLandscape ? done : wait}</span></p>
      </div>
      <Button className={styles.button} onClick={() => document.body.requestFullscreen()}>Enter Fullscreen</Button>
    </PopupOverlay>
  )
}

export default MobileCheck;
