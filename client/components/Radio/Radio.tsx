import { useRef } from "react";
import styles from "./Radio.module.css";

interface RadioProps {
  name: string;
  value: string;
  label: string;
  onChange: (e: string) => void;
  className?: string;
  disabled?: boolean;
  checked?: boolean;
}

const Radio = (props: RadioProps) => {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <div className={styles.Radio}>
      <input
        type="radio"
        onChange={() => props.onChange(props.value)}
        onClick={() => props.onChange(props.value)}
        id={"rad-" + props.value} // to avoid nameclashes with checkbox
        name={props.name}
        value={props.value}
        disabled={props.disabled}
        checked={props.checked}
        ref={ref}
      />
      <label className={styles.label} htmlFor={"rad-" + props.value}>{props.label}</label>
    </div>
  )
};

export default Radio;
