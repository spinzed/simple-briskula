import { useState } from "react";
import { CSSTransition } from "react-transition-group";
import styles from "./IconNotif.module.css";

interface IconNotifProps {
  className?: string;
  icon: string;
  children: React.ReactNode;
}

const IconNotif = (props: IconNotifProps): JSX.Element => {
  const [textShown, setTextShown] = useState(false);

  const onClick = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    e.stopPropagation(); // stop the document click element from firing
    const setFalse = () => {
      setTextShown(false);
      document.removeEventListener("click", setFalse)
    };
    document.addEventListener("click", setFalse);
    setTextShown(true);
  };

  return (
    <div className={`${styles.IconNotif} ${props.className || ""}`}>
      <i className={`${styles.icon} ${props.icon}`} onClick={onClick}></i>
      <CSSTransition
        in={textShown}
        timeout={500}
        mountOnEnter
        unmountOnExit
        classNames={{
          enter: styles.hide,
          enterActive: styles.show,
          enterDone: styles.show,
          exitActive: styles.hide,
        }}
      >
        <div className={styles.text}>{props.children}</div>
      </CSSTransition>
    </div>
  )
}

export default IconNotif;
