import styles from "./GameStartOverlay.module.css";
import { friendlies, opponents } from "../../utils/format";
import StatsOverlay from "../StatsOverlay/StatsOverlay";

interface GameStartOverlayProps {
  in: boolean;
  data?: Data.General;
  onEntered?: () => void;
  onExited?: () => void;
}

const GameStartOverlay = (props: GameStartOverlayProps) => {
  return (
    <StatsOverlay {...props} className={styles.GameStartOverlay}>
      <div>
        <div className={styles.me}>
          {props.data ? friendlies(props.data).join(" & ") : "Loading..."}
        </div>
        <div className={styles.v}>V</div>
      </div>
      <div>
        <div className={styles.opponent}>
          {props.data ? opponents(props.data).join(" & ") : "Loading..."}
        </div>
        <div className={styles.s}>S</div>
      </div>
    </StatsOverlay>
  )
}

export default GameStartOverlay;
