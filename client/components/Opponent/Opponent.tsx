import { useState, useRef, useEffect } from "react";
import Card from "../../components/Card/Card";
import styles from "./Opponent.module.css";
import Fan from "../../components/Fan/Fan";
import useCardController from "../../hooks/cardController";
import { CSSTransition } from "react-transition-group";
import { OPPONENT_CARD_TIME } from "../../game/constants";
import Opacity from "../Opacity/Opacity";

/* NEEDS FIX: Animations are a bit off when a card leaves */

interface OpponentProps {
  delay: number;
  current: boolean;
  player: Data.Player;
  cardNumber: number;
  lastCard?: Game.Card;
}

const mergeStyles = (s1: React.CSSProperties, s2: React.CSSProperties) => {
  const trans = (s1.transform || "") + " " + (s2.transform || "");
  const newObj = Object.assign({}, s1, s2);
  newObj.transform = trans;
  return newObj;
}

const generateCards = (num: number): Game.Card[] => {
  return new Array(num).fill("").map((_, i) => {
    return { sign: "back", id: i };
  });
}

const Opponent: (props: OpponentProps) => JSX.Element = (props) => {
  const [firstRender, setFirstRender] = useState(true);
  const currentStyles = useRef<React.CSSProperties[]>([]);
  const [cards, setCards] = useState(generateCards(props.cardNumber));
  const [shownCards, updateCardStatus] = useCardController(cards);
  const [lastCard, setLastCard] = useState<Game.Card | undefined>(undefined);

  const transitions = {
    transAppear: {
      transform: "translateY(50rem)",
    },

    transAppearActive: {
      transform: "translate(0rem)",
      transition: "all 800ms ease-in",
    },

    transExitActive: {
      transform: "translateY(-10rem)",
      transition: "all 300ms ease-in",
      opacity: "0",
    }
  }

  const setStyle = (s1: React.CSSProperties, index: number) => {
    const newArray = [...currentStyles.current];

    // I am sick of this I just want to get it done
    if (s1 === undefined) {
      newArray[index] = { color: "blue" };
    }

    const standardStyle = { transform: "scale(0.75)" };

    newArray[index] = mergeStyles(standardStyle, s1);
    currentStyles.current = newArray;
  }

  useEffect(() => {
    setCards(generateCards(props.cardNumber));
  }, [props.cardNumber]);

  useEffect(() => {
    const t = setTimeout(() => {
      setLastCard(props.lastCard);
    }, OPPONENT_CARD_TIME);
    return () => clearTimeout(t);
  }, [props.lastCard]);

  const isTeam = props.player.isTeammate;

  return (
    <div className={styles.Opponent}>
      <Fan className={styles.fan}>
        {shownCards.map(({ card, status }, i) => (
          <CSSTransition
            in={status === "shown"}
            timeout={{
              appear: firstRender ? props.delay + 400 : 400,
              enter: firstRender ? props.delay + 400 : 400,
              exit: 400,
            }}
            classNames={{
              appear: styles.transAppear,
              enter: styles.transEnter,
            }}
            appear
            mountOnEnter
            unmountOnExit
            key={card.id}
            onEnter={() => setStyle(transitions.transAppear, i)}
            onEntering={() => setStyle(transitions.transAppearActive, i)}
            onEntered={() => { setStyle({}, i); firstRender && setFirstRender(false) }}
            onExiting={() => setStyle(transitions.transExitActive, i)}
            onExited={() => { setStyle({}, i); updateCardStatus(card) }}
          >
            <Card
              className={styles.Card}
              style={Object.assign({}, currentStyles.current[i])} // this makes 0 fucking sense
              card={{ sign: "back" }}
            />
          </CSSTransition>
        ))}
      </Fan>
      <Opacity
        in={true}
        timeout={500}
        delay={firstRender ? props.delay : 0}
        appear
        mountOnEnter
      >
        <div className={styles.plInfo}>
          <div className={isTeam ? styles.teammate : styles.opponent}>{props.player.name}</div>
          <div className={styles.teamopp}>{isTeam ? "Teammate" : "Opponent"}</div>
          <Opacity
            in={props.current}
            timeout={500}
            delay={firstRender ? props.delay : 0}
            appear
            mountOnEnter
            unmountOnExit
          >
            <div className={styles.teamopp}>Now playing</div>
          </Opacity>
        </div>
      </Opacity>
      <Opacity
        in={props.lastCard && lastCard !== props.lastCard}
        timeout={5000}
        mountOnEnter
        unmountOnExit
      >
        <div className={styles.lastCard}>
          <p>This player picked up a card:</p>
          <Card className={styles.lastCardCard} card={props.lastCard || { sign: "back" }} />
        </div>
      </Opacity>
    </div>
  );
};

export default Opponent;
