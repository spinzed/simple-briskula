import { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import styles from "./PopupOverlay.module.css";
import transitions from "./PopupTransitions.module.css";

interface PopupOverlayProps {
  in: boolean;
  children: React.ReactNode;
  opacity?: number;
  className?: string;
  onClickOutside?: (e: React.MouseEvent) => void;
  onKeyDown?: (e: KeyboardEvent) => void;
}

const PopupOverlay = (props: PopupOverlayProps): JSX.Element => {
  const [render, setRender] = useState(false);
  useEffect(() => {
    if (props.in) setRender(true);
  }, [props.in]);

  const clickHandler = (e: React.MouseEvent) => {
    if (!(e.target as Element).classList.contains(styles.PopupOverlay)) return;
    props.onClickOutside && props.onClickOutside(e);
  }

  useEffect(() => {
    if (!props.onKeyDown) return;
    document.addEventListener("keydown", props.onKeyDown);
    return () => props.onKeyDown && document.removeEventListener("keydown", props.onKeyDown);
  }, [props.onKeyDown])

  const bg = { backgroundColor: `rgba(20, 20, 20, ${props.opacity !== undefined ? props.opacity : 0.75})` };
  
  return (
    render ? 
      <div style={bg} className={styles.PopupOverlay} onClick={clickHandler}>
        <CSSTransition
          in={props.in}
          timeout={500}
          appear
          mountOnEnter
          unmountOnExit
          classNames={transitions}
          onExited={() => setRender(false)}
        >
          <div className={`${styles.PopupOverlayInner} ${props.className || ""}`} style={{ transition: "500ms" }}>
            {props.children}
          </div>
        </CSSTransition>
      </div> :
      <></>
  )
}

export default PopupOverlay;
