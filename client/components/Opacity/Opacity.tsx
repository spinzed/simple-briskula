import { cloneElement } from "react";
import { CSSTransition } from "react-transition-group";
import { CSSTransitionProps } from "react-transition-group/CSSTransition";
import styles from "./Opacity.module.css";

type OpacityProps = CSSTransitionProps & {
  delay?: number;
  children: JSX.Element;
};

const Opacity = (props: OpacityProps): JSX.Element => {
  const chProps = props.children.props;
  const duration = props.timeout as number || 500;
  const style = {
    ...chProps.style,
    transition: `all ${duration}ms ease ${props.delay || 0}ms`,
  };

  return <CSSTransition
    {...props}
    classNames={{
      appear: styles.noOpacity,
      appearActive: styles.opacity,
      appearDone: styles.opacity,
      enter: styles.noOpacity,
      enterActive: styles.opacity,
      enterDone: styles.opacity,
      exitActive: styles.noOpacity,
      exitDone: styles.noOpacity,
    }}>
    {cloneElement(props.children, { ...chProps, style })}
  </CSSTransition>;
}

export default Opacity;
