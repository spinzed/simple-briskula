import { useState } from "react";
import styles from "./Footer.module.css";
import Opacity from "../Opacity/Opacity";

const Footer = () => {
  const [discordBubble, setDiscordBubble] = useState(false);
  const discordName = "Spinzed#9939";

  function onClick(e: React.MouseEvent<HTMLElement, MouseEvent>) {
    e.stopPropagation(); // stop the document click element from firing
    const setFalse = () => {
      setDiscordBubble(false);
      document.removeEventListener("click", setFalse);
    }
    document.addEventListener("click", setFalse);
    setDiscordBubble(true);
  }

  return (
    <div className={styles.Footer}>
      <div className={styles.innerContainer}>
        <a className={`fab fa-github ${styles.icon}`}
          href="https://github.com/spinzed" target="_blank"
          rel="noreferrer" title="Check out my Github!"></a>
        <a className={`fab fa-twitter ${styles.icon}`}
          href="https://twitter.com/spinzed" target="_blank"
          rel="noreferrer" title="Check out my Twitter!"></a>
        <a className={`far fa-window-maximize ${styles.icon}`}
          href="https://dnajev.net" target="_blank"
          rel="noreferrer" title="Check out my website!"></a>
        <a className={`fab fa-discord ${styles.icon}`}
          title="Contact me on Discord!" onClick={onClick}></a>
        <Opacity
          in={discordBubble}
          timeout={200}
          mountOnEnter
          unmountOnExit
        >
          <div className={styles.bubble} onClick={() => {
            navigator.clipboard.writeText(discordName);
          }}>
            <div className={styles.discordName}>{discordName}</div>
            <div className={styles.copy}>Click to copy</div>
          </div>
        </Opacity>
        <div className={styles.creator}>Made by Spinzed</div>
      </div>
    </div>
  )
}

export default Footer;
