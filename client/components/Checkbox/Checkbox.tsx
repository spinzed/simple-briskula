import { useRef } from "react";
import styles from "./Checkbox.module.css";

interface CheckboxProps {
  name: string;
  label: string;
  onChange: (e: boolean) => void;
  className?: string;
  disabled?: boolean;
  checked?: boolean;
}

const Checkbox = (props: CheckboxProps) => {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <div className={styles.Checkbox}>
      <input
        type="checkbox"
        onChange={() => props.onChange(ref.current?.checked || false)}
        id={"check-" + props.name} // to avoid nameclashes with radio
        name={props.name}
        value={props.name}
        disabled={props.disabled}
        checked={props.checked}
        ref={ref}
      />
      <label className={styles.label} htmlFor={"check-" + props.name}>{props.label}</label>
    </div>
  )
};

export default Checkbox;
