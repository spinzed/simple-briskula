import { useEffect, useState } from "react";
import styles from "./EndgameScreen.module.css";
import Title from "../Title/Title";
import Footer from "../Footer/Footer";
import Button from "../Button/Button";
import Opacity from "../Opacity/Opacity";

interface RestartData {
  ready: boolean;
  unavailable: boolean;
  playerNum: number;
  outOf: number;
}

interface EndgameScreenProps {
  data?: Data.GameEnd;
  onMainMenu: () => void;
  onRestartGame: () => void;
}

const rematchLabels = ["Rematch!", "Waiting...", "Success!", "Not available"];

const EndgameScreen = (props: EndgameScreenProps) => {
  const [playerNum, setPlayerNum] = useState<{current: number, max: number} | undefined>(undefined);
  const [restartState, setRestartState] = useState(0); // 0 for nothing, 1 for pending request, 2 for ready, 3 for not available

  // handle restart response
  useEffect(() => {
    if (!props.data?.nsp) return;
    props.data.nsp.on("restart", (restart: RestartData) => {
      // if the restart if unavailable
      if (restart.unavailable) {
        setRestartState(3);
        return;
      }
      // set the data how many players are ready
      setPlayerNum({ current: restart.playerNum, max: restart.outOf });

      // if we sent a request for rematch and the number of ready players have changed, it means that
      // out request was successful. This is not the best way to do things, but it doesn't matter too much.
      restartState == 1 && setRestartState(2);

      // if the restart is ready
      if (restart.ready) {
        props.onRestartGame();
      }
    })
    return () => void props.data?.nsp.off("restart");
  }, []);

  const data = props.data;
  let resultText = "Loading...";

  if (data) {
    data.result !== "draw" ? resultText = `You ${data.result}!` : resultText = "Draw!";
  }

  const onRematchAccept = () => {
    props.data?.nsp.emit("restart", { confirm: true });
    setRestartState(1);
  }

  const onMainMenu = () => {
    props.data?.nsp.off("restart"); // to prevent the player dc message from popping up
    props.data?.nsp.emit("restart", { confirm: false });
    props.onMainMenu();
  }

  return (
    <div className={styles.EndgameScreen}>
      <Title />
      <div className={styles.em4}>{resultText}</div>
      <div className={styles.em3}>
        Your points: {data?.points !== undefined ? data.points : "Loading..."}
        {data?.bele !== undefined && ` (${data.bele} bel${data.bele == 2 ? "e" : "a"})`}
      </div>
      <div className={styles.em2}>Wins/losses: {data?.wins} - {data?.losses}</div>
      <div className={styles.em2}>Total points/opponent's total points: {data?.totalPoints} - {data?.totalEnemyPoints}</div>
      <div className={styles.buttons}>
        <Button
          onClick={onRematchAccept}
          disabled={restartState !== 0}
        >
          {rematchLabels[restartState]}
        </Button>
        <Button onClick={onMainMenu}>Main Menu</Button>
      </div>
      <Opacity in={!!playerNum || restartState == 3} timeout={500}>
        <div className={styles.reconn}>
          {restartState == 3 ?
            "One or more players has disconnected, restart not available." :
            `Players ready: ${playerNum?.current} / ${playerNum?.max}`
          }
        </div>
      </Opacity>
      <Footer />
    </div>
  )
};

export default EndgameScreen;
