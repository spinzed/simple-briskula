import { useState, useEffect } from "react";
import Card from "../Card/Card";
import styles from "./Hand.module.css";
import Chat from "../Chat/Chat";
import CardTransitions from "./CardTransitions.module.css";
import { CSSTransition } from "react-transition-group";
import useCardController from "../../hooks/cardController";
import Button from "../Button/Button";
import { Socket } from "socket.io-client";
import { DefaultEventsMap } from "@socket.io/component-emitter";
import { game } from "../../utils/game";
import { isPhone } from "../../utils/client";
import { MAX_CHAT_MESSAGES } from "../../game/constants";

interface HandProps {
  delay: number;
  cards: Game.Card[];
  isMyTurn: boolean;
  nsp: Socket<DefaultEventsMap, DefaultEventsMap> | undefined;
  viewingTeammate: boolean;
  onSwitchCards: () => void;
  name?: string;
  masterSign?: string; // treseta only, the sign of the first thrown card
}

interface MessageData {
  player: string;
  message: string;
}

const Hand: (props: HandProps) => JSX.Element = (props) => {
  const [selectedCard, setSelectedCard] = useState<string | undefined>(undefined);
  const [history, setHistory] = useState<MessageData[]>([]);
  const [shownCards, updateCardStatus] = useCardController(props.cards);
  const [firstRender, setFirstRender] = useState(true);

  const restrictSign = game() === "treseta" && props.masterSign && props.cards.some(c => c.sign === props.masterSign);

  // TODO: refractor chat handling
  const addHistoryData = (data: MessageData) => {
    const historyNew = [...history];
    historyNew.push(data);

    if (historyNew.length > MAX_CHAT_MESSAGES) historyNew.shift();
    setHistory(historyNew);
  };

  useEffect(() => {
    props.nsp && props.nsp.on("chat", (data: { holder: string, message: string }) => {
      addHistoryData({ player: data.holder, message: data.message });
    });
    return () => { props.nsp && props.nsp.off("chat"); };
  }, [history]);

  const available = (card: Game.Card) => {
    return !props.viewingTeammate && props.isMyTurn && !(restrictSign && props.masterSign !== card.sign);
  }

  const onDoubleClick = (card: Game.Card) => {
    if (!available(card)) return;
    updateCardStatus(card);
    setSelectedCard(undefined);

    props.nsp && props.nsp.emit("gameUpdate", {
      action: "playCard",
      payload: { card }
    });
  };

  const handleMessageSend = (message: string) => {
    addHistoryData({ player: "You", message });
    props.nsp && props.nsp.emit("chat", { message });
  }

  return (
    <div className={styles.Hand}>
      <div className={`${styles.cardContainer} ${isPhone() && game() === "treseta" ? styles.tresetaPhone : ""}`}>
        {shownCards.map(({ card, status }) => (
          <CSSTransition
            in={status === "shown"}
            timeout={{
              appear: 800 + (firstRender ? props.delay : 0) ,
              enter: 800 + (firstRender ? props.delay : 0),
              exit: 300
            }}
            appear
            mountOnEnter
            unmountOnExit
            classNames={CardTransitions}
            key={card.sign + card.number}
            onEntered={() => setFirstRender(false)}
          >
            <div style={{ transitionDelay: firstRender ? props.delay + "ms" : "" }}>
              <Card
                onCSSHover={styles.CardHover}
                className={`${styles.Card} ${(card.sign + card.number) === selectedCard && styles.selected}`}
                card={card}
                onClick={() => available(card) && setSelectedCard(card.sign + card.number)}
                onGesture={isPhone() ? (gesture) => {
                  gesture === "up" && onDoubleClick(card);
                } : undefined}
                block={(!props.isMyTurn && "Wait for your turn!") || (restrictSign && props.masterSign !== card.sign && "Cannot play this card!") || undefined}
                onDoubleClick={() => onDoubleClick(card)} />
            </div>
          </CSSTransition>
        ))}
        {props.viewingTeammate &&
          <div className={styles.teammate}>
            <p>Teammate's Cards</p>
            <Button size="small" onClick={props.onSwitchCards}>Switch Back</Button>
          </div>
        }
      </div>
      <Chat history={history} onEnter={handleMessageSend} name={props.name} />
    </div>
  );
};

export default Hand;
