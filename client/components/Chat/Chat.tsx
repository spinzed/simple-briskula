import { useState, useRef, useEffect } from "react";
import { MAX_CHAT_MESSAGES } from "../../game/constants";
import styles from "./Chat.module.css";

interface ChatProps {
  className?: string;
  onEnter: (message: string) => void;
  history: { player: string, message: string }[];
  name?: string;
}

const Chat = (props: ChatProps) => {
  const [text, setText] = useState("");
  const historyDiv = useRef<HTMLDivElement>(document.createElement("div"));
  const historyUpdate = useRef(props.history);

  useEffect(() => {
    if (props.history !== historyUpdate.current) {
      const elm = historyDiv.current;
      elm.scrollTop = elm.scrollHeight;
      historyUpdate.current = props.history;
    }
  });

  const st = {
    height: `${100 / MAX_CHAT_MESSAGES}%`,
    fontSize: `${10 / MAX_CHAT_MESSAGES}rem`,
  };

  return (
    <div className={`${styles.Chat} ${props.className || ""}`}>
      <div className={styles.history} ref={historyDiv}>
        {props.history.map((entry, i) => (
          <div className={styles.entry} style={st} key={i}>
            <div className={styles.playerName}>{entry.player}:</div>
            <div className={styles.message}>{entry.message}</div>
          </div>
        ))}
      </div>
      <div className={styles.inputContainer}>
        <input
          className={styles.input}
          type="text"
          value={text}
          maxLength={50}
          placeholder={`Chat here${props.name ? ` as "${props.name}"` : "..."}`}
          onChange={e => setText(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              setText("");
              text !== "" && props.onEnter(text);
            }
          }}
        />
      </div>
    </div>
  )
}

export default Chat;
