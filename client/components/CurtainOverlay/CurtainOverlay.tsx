import { cloneElement } from "react";
import styles from "./CurtainOverlay.module.css";
import transitions from "./CurtainTransitions.module.css";
import { CSSTransition } from "react-transition-group";
import { theme } from "../../utils/game";

export interface CurtainOverlayProps {
  in: boolean;
  children?: JSX.Element[];
  className?: string;
  onEntered?: () => void;
  onExited?: () => void;
  data?: Data.General;
}

const CurtainOverlay = (props: CurtainOverlayProps) => {
  const settings = {
    in: props.in,
    timeout: 1000,
    appear: true,
    mountOnEnter: true,
    unmountOnExit: true,
  };

  const leftnright = {
    transition: "1000ms",
    backgroundColor: theme.darkBackground.rgb
  }

  const classNames = [styles.left, styles.right, styles.static];
  const customStyles = [leftnright, leftnright, {}];
  const ch = props.children;

  // this is ugly af
  const result: (JSX.Element | undefined)[] = (ch || []).map((child, i) => {
    if (i > 3) {
      i === 3 && console.error("too many children");
      return;
    }
    const newProps = Object.assign({}, child.props);

    newProps.className = classNames[i];
    newProps.style = customStyles[i];

    return cloneElement(child, newProps);
  });

  return (
    <CSSTransition {...Object.assign({ ...settings }, { onEntered: props.onEntered, onExited: props.onExited })}>
      <div className={`${styles.CurtainOverlay} ${props.className || ""}`}>
        <CSSTransition
          {...settings}
          classNames={{
            appear: transitions.leftEnter,
            appearActive: transitions.leftEnterActive,
            appearDone: transitions.leftEnterDone,
            exit: transitions.exit,
            exitActive: transitions.leftExitActive
          }}
        >
          {result[0]}
        </CSSTransition>
        <CSSTransition
          {...settings}
          classNames={{
            appear: transitions.rightEnter,
            appearActive: transitions.rightEnterActive,
            appearDone: transitions.rightEnterDone,
            exit: transitions.exit,
            exitActive: transitions.rightExitActive
          }}
        >
          {result[1]}
        </CSSTransition>
        <CSSTransition {...Object.assign({ ...settings }, { in: ch && ch.length > 3 && props.in })}>
          {result[2] || <></>}
        </CSSTransition>
      </div>
    </CSSTransition>
  )
}

export default CurtainOverlay;
