import { useState, useRef, useEffect, useLayoutEffect } from "react";
import React, { useContext } from "react";
import styles from "./JoinMenu.module.css";
import InputTransitions from "./InputTransitions.module.css";
import Title from "../../components/Title/Title";
import Footer from "../../components/Footer/Footer";
import InputField from "../../components/InputField/InputField";
import { CSSTransition } from "react-transition-group";
import Button from "../../components/Button/Button";
import SlickInputField from "../../components/SlickInputField/SlickInputField";
import CookieBanner from "../CookieBanner/CookieBanner";
import Cookies from "js-cookie";
import Header from "../Header/Header";
import { makeNsp, SocketCtx } from "../../socket/socket";
import { getGameIDAndKey, getModeEnabled, isAllowed, resetKeyAndGameID, setIsAllowed, setModeEnabled, setSavedName } from "../../utils/cookie";
import FAQ from "../FAQ/FAQ";
import { getByModeName, Modes } from "../../game/constants";
import Checkbox from "../Checkbox/Checkbox";
import Radio from "../Radio/Radio";
import PopupOverlay from "../PopupOverlay/PopupOverlay";
import IconNotif from "../IconNotif/IconNotif";
import { game, relevantModes, theme } from "../../utils/game";
import { isPhone } from "../../utils/client";
import img from "../../static/images/background.webp";
import { capitalFirst } from "../../utils/format";
import Opacity from "../Opacity/Opacity";
import PopupMenu from "../PopupMenu/PopupMenu";

// odgovor servera kad mu se posalje upit za queue join, kreiranje lobija, joinanje lobija ili
// kada je igra spremna STO NIJE ODGOVOR
interface Response {
  type: string; // joinedQueueStatus | makeLobby | joinLobby | gameReady
  payload: {
    name: string;
    success: boolean;
    wasInQueue?: boolean;
    lobbyID?: string;
    mode?: string;
    uuid?: string;
    playerID?: string;
    key? : string;
    startTime?: number;
    players?: Data.Player[];
    reason?: string;
    playerIn?: { id: string, name: string };
    playerOut?: { id: string, name: string };
  }
}

// moguca stanja u kojima je se moguce naci u meniju
type Status = 
  "inQueue" | "lobbyCreateIn" | "lobbyJoinIn" | "lobbyGameStarting" | "queueWaiting" | "lobbyCreateWaiting" | "lobbyJoinWaiting" |
  "lobbyJoinEntering" | "lobbyCreateEntering" |
  "queueError" | "queueNoName" | "queueNoneSelected"  | "lobbyCreateError" | "lobbyJoinError"| "lobbyCreateNoName" | "lobbyJoinNoName" | "lobbyJoinNoLobby" |
  "idle" |
  "reconnAvailable" | "reconnecting" |
  "ready";

// svaki status pripada jednom od ovih tipova, funkcija ispod odreduje koji tip pripada kojem
type StatusType = "waiting" | "entering" | "error" | "idle" | "ready";

// odredi StatusType od statusa Status
function getStatusType(status: Status): StatusType {
  if (["inQueue", "lobbyCreateIn", "lobbyJoinIn", "queueWaiting", "lobbyCreateWaiting", "lobbyJoinWaiting"].includes(status)) {
    return "waiting";
  }
  if (["lobbyJoinEntering", "lobbyCreateEntering"].includes(status)) {
    return "entering";
  }
  if (["queueError", "queueNoName", "queueNoneSelected", "lobbyCreateError", "lobbyJoinError", "lobbyJoinNoLobby"].includes(status)) {
    return "error";
  }
  if (["idle"].includes(status)) {
    return "idle";
  }
  if (["reconnAvailable", "lobbyGameStarting", "reconnecting"].includes(status)) {
    return "ready";
  }
  if (["ready"].includes(status)) {
    return "ready";
  }
  // should never execute
  console.error("unknown status:", status)
  return "error"
}

type TeamPlayer = { id: string, name: string };
type Team = TeamPlayer[];

interface JoinMenuProps {
  onGameReady: (data: Data.General) => void;
}

const JoinMenu = (props: JoinMenuProps): JSX.Element => {
  const [name, setName] = useState(Cookies.get("name") || "");
  const [gameStatus, setGameStatus] = useState<Status>("idle");
  const [lobbyID, setLobbyID] = useState<string | undefined>(undefined);
  const [enteredLobbyID, setEnteredLobbyID] = useState<string>("");
  const [cookie, setCookie] = useState<boolean | undefined>(isAllowed());
  const [modesEnabled, __setModesEnabled] = useState<{[m: string]: boolean}>(() => {
    const modes: {[m: string]: boolean} = {};
    relevantModes().forEach(m => modes[m.name] = getModeEnabled(m.name));
    return modes;
  });
  const [radioMode, setRadioMode] = useState<string>(() => relevantModes()[0].name);
  const [teamMode, setTeamMode] = useState<"random" | "pickTeams">("random");
  const [plInLobby, setPlInLobby] = useState<Team>([]);
  const [teams, setTeams] = useState<Team[]>([]);
  const socket = useContext(SocketCtx);
  // if its false, you can send data, if its true, you cannot
  const rateLimiter = useRef(false);
  
  const waitingGame = getStatusType(gameStatus) === "waiting";

  const setModesEnabled = (modeName: string, v: boolean) => {
    modesEnabled[modeName] = v;
    __setModesEnabled({...modesEnabled});
    setModeEnabled(modeName, v)
  }

  useLayoutEffect(() => {
    // if none of the modes are selected at first, select the normal
    // 1v1 mode but before the first layout has been painted
    if (Object.keys(modesEnabled).every(m => !getModeEnabled(m))) {
      const first = Object.keys(modesEnabled)[0];
      setModesEnabled(first, true);
    }
  }, []);

  useEffect(() => {
    const [id, key] = getGameIDAndKey();
    
    if (!id || !key) {
      // in case just one of them was defined for some reason
      resetKeyAndGameID();
      return;
    }
    console.info("found id and key", id, key);

    socket.emit("gameStart", { type: "reconnect", payload: { id, key } });
  }, []);

  /* 
    ===================================
    SOCKET LISTENER ZA ODGOVORE SERVERA
    ===================================
  */

  useEffect(() => {
    socket.on("gameStart", (response: Response) => {
      if (response.type === "joinQueue") {
        setGameStatus(response.payload.success ? "inQueue" : "queueError");
        !response.payload.success && console.warn("[WARN] Failed to join the queue");
        console.info("[INFO] Name:", name);
      } else if (response.type === "leaveQueue") {
        setGameStatus("idle");
        response.payload.wasInQueue && console.warn("[WARN] The player was not in queue");
        console.info("[INFO] Name:", name);
      } else if (response.type === "makeLobby") {
        if (!response.payload.lobbyID) console.error("[ERROR] LobbyID should be defined but it's not");
        setLobbyID(response.payload.lobbyID);
        setGameStatus(response.payload.success ? "lobbyCreateIn" : "lobbyCreateError");
      } else if (response.type === "joinLobby") {
        if (response.payload.success) {
          setGameStatus("lobbyJoinIn");
        } else {
          // ukljuci rate limiter posto je unesen krivi lobby ID
          rateLimiter.current = true;
          if (response.payload.reason === "err") {
            console.error("Cannot join lobby:", response.payload.reason);
            setGameStatus("lobbyJoinError");
          } else {
            setGameStatus("lobbyJoinNoLobby");
          }
        }
      } else if (response.type === "lobbyUpdate") {
        console.info("[Game] Lobby player update");
        const plIn = response.payload.playerIn;
        const plOut = response.payload.playerOut;
        let newPlInLobby = [...plInLobby];
        let newTeams = teams.map(team => [...team]);

        if (plIn) {
          newPlInLobby.push(plIn);

          if (teams.length !== 0) {
            let leastTeam = newTeams[0];
            newTeams.forEach(team => { if (team.length < leastTeam.length) leastTeam = team; });
            leastTeam.push(plIn);
          }
        }
        if (plOut) {
          newPlInLobby = newPlInLobby.filter(p => p.id !== plOut.id);
          if (teams.length !== 0) {
            newTeams = newTeams.map(team => team.filter(p => p.id !== plOut.id));
          }
        }
        setPlInLobby(newPlInLobby);
        teams.length !== 0 && setTeams(newTeams);
      } else if (response.type === "startLobbyGame") {
        if (response.payload.success) {
          setGameStatus("lobbyGameStarting")
        }
      } else if (response.type === "gameReady") {
        const { mode, uuid, playerID, startTime, key, players } = response.payload;

        if (!mode || !uuid || !playerID || !key || !startTime || !players) {
          console.error("Unset data on game ready:", mode, uuid, playerID, key, players);
          return;
        }
        const realMode = relevantModes().find(m => m.name === mode);
        if (!realMode) {
          console.error("Unsupported mode:", mode);
          return;
        }

        console.info("[Game] Game mode:", mode);
        setGameStatus("ready");

        const data = { ...response.payload, name, mode: realMode }
        props.onGameReady(data as Data.General);
      } else if (response.type === "reconnect") {
        const p = response.payload; // success, mode, name, playerID, players

        // if it isn't possible to reconnect to the game, clear
        // the game data stored in cookies
        if (!p.success) {
          console.info("Reconnect not available")
          resetKeyAndGameID();
          return;
        }
        setGameStatus("reconnAvailable");

        console.info("Reconnect available");
      } else {
        console.error("fatal: unexpected game response type", response.type);
      }
    });

    return () => { socket.off("gameStart") };
  }, [name, plInLobby, teams]); // depends on name

  /* 
    ==============
    INPUT HANDLERI
    ==============
  */

  // name change
  const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    rateLimiter.current = false;
    setName(e.target.value);
  }

  // lobby ID change
  const onLobbyIDChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    rateLimiter.current = false;
    setEnteredLobbyID(e.target.value);
  }

  // da/ne botuni na cookie banneru
  const onCookieChange = (res: boolean) => {
    setCookie(res); 
    setIsAllowed(res);

    // if the result was true and name is neither undefined or "", save it
    res && !name && setSavedName(name);
  }

  /* 
    ===================
    HANDLERI ZA BUTTONE
    ===================
  */

  // matchmaking botun
  const onQueuePress = () => {
    // promijeni funkciju botuna kada je nesto lobby 
    if (gameStatus.includes("lobby")) {
      setGameStatus("idle");
      return;
    }

    // ako je player u queueu, izbaci ga iz njega
    if (gameStatus === "inQueue") {
      socket.emit("gameStart", { type: "leaveQueue" });
      return;
    }

    if (name === "") {
      setGameStatus("queueNoName");
      return;
    }

    setGameStatus("queueWaiting");
    setLobbyID(undefined);
    setSavedName(name);

    const modes: string[] = relevantModes().filter(m => modesEnabled[m.name]).map(m => m.name);

    // if none modes are selected, don't emit a message and
    // display message to the user
    if (modes.length === 0) {
      setGameStatus("queueNoneSelected");
      return;
    }

    socket.emit("gameStart", {
      type: "joinQueue",
      payload: { name, modes }
    });
  }

  // input polje
  const onQueueInput = (e: React.KeyboardEvent<HTMLInputElement>) => {
    // if not enter, return
    if (e.key !== "Enter") return;

    // call the same function that is called when the queue button is pressed
    onQueuePress();
  }

  // join lobby botun
  const onLobbyJoinPress = () => {
    setGameStatus("lobbyJoinEntering");
    setLobbyID(undefined);
    setSavedName(name);
  }

  // join lobby ID polje ispod botuna
  const onJoinLobbyWithID = (e: React.KeyboardEvent<HTMLInputElement>) => {
    // if not enter or nothing is entered
    if (e.key !== "Enter" || !enteredLobbyID || rateLimiter.current) return;

    if (name === "") {
      setGameStatus("lobbyJoinNoName");
      return;
    }

    setGameStatus("lobbyJoinEntering");

    socket.emit("gameStart", {
      type: "joinLobby",
      payload: { name, lobbyID: enteredLobbyID }
    });
  }

  // make lobby botun
  const onLobbyMakePress = () => {
    setGameStatus("lobbyCreateEntering");
    setLobbyID(undefined);
    setSavedName(name);
  };

  // mali botun ispod radio make lobby botuna
  const onLobbySendReq = () => {
    if (name === "") {
      setGameStatus("lobbyCreateNoName");
      return;
    }
    setGameStatus("lobbyCreateWaiting");

    // make space for players in the teams array according to the mode
    const modeData = getByModeName(radioMode);
    if (modeData && modeData.maxPlayers > 2) {
      setTeams(new Array(modeData.maxTeams).fill([]));
    }

    socket.emit("gameStart", {
      type: "makeLobby",
      payload: { name, mode: radioMode }
    });
  }

  // reconnect botun
  const onReconnect = () => {
    rateLimiter.current = true;

    // at this point, these have to be defined
    const [id, gameKey] = getGameIDAndKey();
    const nsp = makeNsp(id || "", gameKey);

    nsp.on("reconn", d => {
      if (!d.mode || !d.name || !d.playerID || !d.startTime || !d.players || !d.dcd) {
        console.error("Unset data on reconnect:", d.mode, d.name, d.playerID, d.startTime, d.players, d.dcd);
      }
      const realMode = Object.values(Modes).find(m => m.name === d.mode);
      if (!realMode) {
        console.error("Unsupported mode:", d.mode);
        return;
      }
      // reconstruct the data and start the game
      const data = {
        mode: realMode,
        uuid: id,
        name: d.name,
        playerID: d.playerID,
        key: gameKey,
        startTime: d.startTime,
        players: d.players,
        dcd: d.dcd,
        nsp
      } as Data.General;

      // remove all listeners created here
      nsp.off("reconn");

      props.onGameReady(data);
    });
    setGameStatus("idle");
  }

  // stay in lobby botun
  const onStayInLobby = () => {
    resetKeyAndGameID();
    setGameStatus("idle");
  }

  // start lobby botun
  const startLobbyGame = () => {
    // ako nije isto igraca u timovima, nemoj napravit nista
    if (teams[0]?.length !== teams[1]?.length) return;
    socket.emit("gameStart", {
      type: "startLobbyGame",
      payload: { opts: { random: teamMode === "random", teams: teams.length > 0 && teams.map(t => t.map(p => p.id)) } }
    });
  }

  // switchaj team za igraca kad kliknes na njega
  // tim je refrence za postojeci array
  const handleSwitchTeam = (player: TeamPlayer, teamIndex: number) => {
    // ne radi nista ako nije enableano switchanje tima
    if (teamMode !== "pickTeams") return;

    // izbaci ga iz trenutnog tima i prebaci ga u drugi
    teams[teamIndex] = teams[teamIndex].filter(p => p.id !== player.id);
    teams[(teamIndex + 1) % 2].push(player);
    setTeams([...teams]);
  }

  /* 
    ==================
    FUNKCIJE ZA LABELE
    ==================
  */

  const getQueueLabel = () => {
    const labels: { [key: string]: string} = {
      default: "Cancel",
      idle: "Find Game",
      queueNoName: "Invalid Name",
      queueError: "Unexpected Error",
      queueNoneSelected: "No Modes Selected",
      queueWaiting: "Waiting...",
      inQueue: "Cancel Queue"
    }

    return labels[gameStatus] || labels.default;
  };

  const getMakeLobbyLabel = () => {
    const labels: { [key: string]: string} = {
      default: "Create Lobby",
      lobbyCreateNoName: "Invalid Name",
      lobbyCreateError: "Unexpected Error",
      lobbyCreateEntering: "Waiting...",
      lobbyCreateWaiting: "Waiting...",
      lobbyCreateIn: "Lobby Created!"
    }

    return labels[gameStatus] || labels.default;
  };

  const getJoinLobbyLabel = () => {
    const labels: { [key: string]: string} = {
      default: "Join Lobby",
      lobbyJoinNoName: "Invalid Name",
      lobbyJoinNoLobby: "Invalid Lobby Name",
      lobbyJoinError: "Unexpected Error",
      lobbyJoinEntering: "Waiting...",
      lobbyJoinWaiting: "Waiting...",
      lobbyJoinIn: "Lobby Joined!"
    }

    return labels[gameStatus] || labels.default;
  };

  // css url() is broken since updating file-loader/css-loader, this is a workaround
  const st = {
    background: `linear-gradient(149deg, ${theme.background.rgba(1)} 69%, ${theme.background.rgba(0.8547794117647058)} 89%, ${theme.background.rgba(0.5410539215686274)} 100%), url(${img})`,
    backgroundSize: "cover",
    backgroundPosition: "bottom right",
  };

  return (
    <div className={styles.JoinMenu} style={st}>
      <Header />
      {!isPhone() && <FAQ />}
      <div className={styles.fillerBox} style={{ alignItems: "center", justifyContent: "flex-end" }}>
        <Opacity
          in={!gameStatus.toLowerCase().includes("lobby")}
          timeout={300}
        >
          <div className={styles.modes}>
            <h1>Modes</h1>
            {relevantModes().map((m) => (
              <Checkbox key={m.name} name={m.name} label={m.showName} 
                onChange={v => setModesEnabled(m.name, v)}
                checked={modesEnabled[m.name]} disabled={waitingGame} />
            ))}
          </div>
        </Opacity>
        {game() !== "treseta" && <IconNotif className={styles.gameNotif} icon={styles.t}>
          <strong>New! </strong>Check out our new project - <a href="https://treseta.dnajev.net" target="_blank" rel="noreferrer" title="Check out my website!">Simple Treseta</a>! 
        </IconNotif>}
        {!isPhone() && <IconNotif className={styles.phoneNotif} icon="fas fa-mobile-alt">
          <strong>{"Experimental: "}</strong>Simple {capitalFirst(game())} is now available for mobile devices! Check it out on your phone right now!
        </IconNotif>}
      </div>
      <div className={styles.verticalBox}>
        <Title />
        <CSSTransition
          in={true}
          appear
          timeout={4000}
          classNames={InputTransitions}
        >
          <InputField
            disabled={waitingGame}
            onChange={onNameChange}
            onKeyDown={onQueueInput}
            value={name}
          />
        </CSSTransition>
        <Button onClick={onQueuePress} disabled={waitingGame && gameStatus !== "inQueue"}>
          {getQueueLabel()}
        </Button>
        <div className={styles.buttons}>
          <Button onClick={onLobbyJoinPress} disabled={waitingGame}>
            {getJoinLobbyLabel()}
          </Button>
          <Button onClick={onLobbyMakePress} disabled={waitingGame}>
            {getMakeLobbyLabel()}
          </Button>
        </div>
          <div className={styles.extra}>
            <Opacity
              in={gameStatus.includes("lobbyJoin")}
              timeout={300}
              appear
              unmountOnExit
              mountOnEnter
            >
              <div className={styles.vert}>
                <div>Enter the lobby ID (Enter to send):</div>
                <SlickInputField
                  onChange={onLobbyIDChange}
                  onKeyDown={onJoinLobbyWithID}
                  placeholder={"Lobby ID"}
                  disabled={waitingGame}
                />
              </div>
            </Opacity>
          </div>
      </div>
      <div className={styles.fillerBox}></div>
      <CookieBanner show={cookie === undefined} onCookieConsent={onCookieChange} />
      <Footer />
      <PopupMenu
        in={gameStatus.includes("lobbyCreate") || gameStatus === "lobbyCreateEntering"}
        onExit={() => gameStatus !== "lobbyCreateIn" && setGameStatus("idle")}
        className={styles.PopupOverlay}
      >
        <div>
          {relevantModes().map((m) => (
            <Radio key={m.name} name="lobbyMode" value={m.name} label={m.showName}
              onChange={setRadioMode} checked={radioMode === m.name} disabled={waitingGame} />
          ))}
        </div>
        {radioMode.includes("2v2") && 
          <div>
            <Radio name="teamMode" value="random" label="Random Teams"
              onChange={() => setTeamMode("random")} checked={teamMode === "random"} disabled={waitingGame} />
            <Radio name="teamMode" value="pickTeams" label={"Pick Teams"}
              onChange={() => setTeamMode("pickTeams")} checked={teamMode === "pickTeams"} disabled={waitingGame} />
          </div>
        }
        {gameStatus !== "lobbyCreateIn" &&
          <Button size="small" disabled={rateLimiter.current} onClick={onLobbySendReq}>
            {gameStatus === "lobbyCreateWaiting" ? "Creating..." : "Create"}
          </Button>
        }
        {gameStatus === "lobbyCreateIn" && 
          <>
            <b>Lobby ID: {lobbyID}</b>
            <div>Waiting For Players...</div>
            {radioMode.includes("2v2") && teamMode === "pickTeams" && 
              <div>
                {teams.map((team, i) =>(
                  <div key={i} className={styles.column}>
                    <b>Team {i+1}</b>
                    {team.map(p => <div key={p.id} className={styles.pointer} onClick={() => handleSwitchTeam(p, i)}>{p.name}</div>)}
                  </div>
                ))}
              </div>
            }
            {radioMode.includes("2v2") && teamMode !== "pickTeams" && 
              <div className={styles.column}>
                <b>Joined Players</b>
                {plInLobby.map((p) =><div key={p.id} className={styles.vert}>{p.name}</div>)}
              </div>
            }
            {radioMode.includes("2v2") && plInLobby.length === getByModeName(radioMode)?.maxPlayers && (
              <Button size="small" onClick={startLobbyGame}>Start Game</Button>
            )}
          </>
        }
      </PopupMenu>
      <PopupOverlay in={gameStatus === "reconnAvailable"} opacity={0.92}>
        <h1 className={styles.title}>Reconnect available!</h1>
        <p>Do you wish to reconnect to the last game?</p>
        <div className={styles.overlayBtns}>
          <Button onClick={onStayInLobby}>Stay in Lobby</Button>
          <Button onClick={onReconnect}>Reconnect</Button>
        </div>
      </PopupOverlay>
    </div>
  );
};

export default JoinMenu;
