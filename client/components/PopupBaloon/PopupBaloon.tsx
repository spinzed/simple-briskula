import Opacity from "../Opacity/Opacity";

const PopupBaloon = () => {
  return (
    <Opacity in={true} timeout={500}>
      <div>Placeholder</div>
    </Opacity>
  )
}

export default PopupBaloon;
