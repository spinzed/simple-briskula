import styles from "./StatsOverlay.module.css";
import CurtainOverlay, { CurtainOverlayProps } from "../CurtainOverlay/CurtainOverlay";
import { capitalFirst } from "../../utils/format";
import { game } from "../../utils/game";

const StatsOverlay = (props: CurtainOverlayProps) => {
  const ch = props.children;
  const c1 = ch && ch.length > 0 ? ch[0] : <></>;
  const c2 = ch && ch.length > 1 ? ch[1] : <></>;

  return (
    <CurtainOverlay {...props}>
      <div>
        {c1.props?.children}
        <div className={styles.stats}>
          <div className={styles.type}>{capitalFirst(game())}</div>
          <div className={styles.mode}>{props.data ? props.data.mode.showName : "Loading..."}</div>
        </div>
      </div>
      <div>
        {c2.props?.children}
      </div>
    </CurtainOverlay>
  )
};

export default StatsOverlay;
