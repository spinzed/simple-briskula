import { cloneElement } from "react";
import styles from "./Fan.module.css";
import { CSSTransition } from "react-transition-group";

interface FanProps {
  className?: string;
  children: JSX.Element[];
}

const Fan = (props: FanProps): JSX.Element => {
  const { children } = props;
  const razmak = 40 - 40 / (Math.log(50) / Math.log(children.length));
  let start = - (children.length - 1) * (razmak / 2);

  return (
    <div className={`${styles.Fan} ${props.className}`}>
      <div className={styles.FanInner} style={{ transform: "translateY(8rem)" }}>
        {children.map((card) => {
          let props: (Partial<any> & React.Attributes); // this is an intresting type, tnx vscode

          if (card.type === CSSTransition) {
            props = Object.assign({}, card.props.children.props);
          } else {
            props = Object.assign({}, card.props);
          }

          if (!props.style) props.style = {};
          const trans = props.style.transform || "";

          const newTrans = trans.replace("   ", " rotate(" + start + "deg) translateY(-10rem) ");

          if (newTrans === trans) {
            props.style.transform = newTrans + " rotate(" + start + "deg) translateY(-10rem)";
          } else {
            props.style.transform = newTrans;
          }

          start += razmak;
          return cloneElement(card, props);
        })}
      </div>

    </div>
  );
};

export default Fan;
