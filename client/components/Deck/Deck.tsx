import { useState, useEffect } from "react";
import styles from "./Deck.module.css";
import Card from "../Card/Card";
import useDescendingCounter from "../../hooks/descendingCounter";
import { CSSTransition } from "react-transition-group";
import cardTransitionsDown from "./cardTransitionsDown.module.css";
import cardTransitionsUp from "./cardTransitionsUp.module.css";

interface DeckProps {
  delay: number;
  className?: string;
  numberOfCards: number;
  briskula?: Game.Card;
  lastWon: boolean;
}

const Deck = (props: DeckProps) => {
  const [lastCards, updateLastCards] = useDescendingCounter(props.numberOfCards);
  const [firstRender, setFirstRender] = useState(true);

  // empty card is just a placeholder until a carrd from bacvkend arrives
  const [briskula, setBriskula] = useState<Game.Card>({ sign: "back" });
  const currentCards = props.numberOfCards;

  const finalCardNo = props.briskula ? lastCards + 1 : lastCards;

  const cardsArray = new Array(finalCardNo).fill("");
  const diff = lastCards - currentCards;

  let direction: "up" | "down" = props.lastWon ? "up" : "down";

  // update briskula if it exists
  useEffect(() => {
    if (props.briskula) setBriskula(props.briskula);
  }, [props.briskula]);

  const cards = cardsArray.map((_, i) => {
    if (i === 0 && (briskula || props.briskula)) {
      const briskula_delay = firstRender ? props.briskula ? props.delay - 800 : 500 : 0;

      return (
        <CSSTransition
          in={!!props.briskula}
          timeout={{
            appear: 2500 + props.delay,
            enter: 2500 + props.delay,
            exit: 500
          }}
          appear
          mountOnEnter
          key={i}
          classNames={{
            appear: styles.briskulaAppear,
            appearActive: styles.briskulaAppearActive,
            exitActive: styles.briskulaAppear,
            exitDone: styles.briskulaAppear
          }}
        >
          <Card
            className={styles.briskulaCard}
            style={{ transitionDelay: `${briskula_delay}ms` }}
            styleInner={{ transitionDelay: `${briskula_delay}ms` }}
            onCSSHover={styles.briskulaCardHover}
            card={props.briskula || briskula}
          />
        </CSSTransition>
      );
    }

    let delay = 0;

    if (firstRender) {
      delay = i * 40 + props.delay;
    } else {
      delay = (i > currentCards ? diff - i + currentCards + 1 : 0) * 500;
    }

    if (i > currentCards) {
      direction === "up" ? direction = "down" : direction = "up";
    }


    // delay += props.delay;

    return (
      <CSSTransition
        in={i <= currentCards}
        timeout={{
          appear: 2500 + props.delay,
          enter: 2500 + props.delay,
          exit: 2500
        }} // had to set it manually cuz it tends to be early
        appear
        unmountOnExit
        classNames={direction === "up" ? cardTransitionsUp : cardTransitionsDown}
        key={i}
        onExited={() => updateLastCards()}
        onEntered={() => {
          if (firstRender && i === finalCardNo - 1) {
            setFirstRender(false);
          }
        }}
      >
        <Card
          style={{
            transform: "translateY(-" + i * 0.1 + "rem)",
            transitionDelay: `${delay - 800}ms`
          }}
          styleInner={{ transitionDelay: `${delay - 800}ms` }}
          className={styles.Card}
          card={{ sign: "back" }}
        />
      </CSSTransition>
    );
  });

  return (
    <div className={props.className}>
      {cards}
    </div>
  )
}

export default Deck;
