/* eslint-disable @typescript-eslint/no-explicit-any */
import { useRef, useEffect } from "react";
import styles from "./Card.module.css";
import Sprites from "../../utils/sprites";
import { onGesture } from "../../utils/gestures";
import { isPhone } from "../../utils/client";

interface ICardProps {
  card: Game.Card;
  className?: string;
  style?: React.CSSProperties;
  styleInner?: React.CSSProperties; // this was inevitable
  // onHover must be like this: .hoverClassName:hover ~ div
  onCSSHover?: any;
  block?: string;
  onClick?: () => any;
  onDoubleClick?: () => any;
  onGesture?: (g: string) => any;
}

const Card: (props: ICardProps) => JSX.Element = (props) => {
  const img = Sprites[(props.card.sign + (props.card.number || "")) as never];
  const cardRef = useRef<HTMLDivElement>(null);
  
  useEffect(() => {
    if (!props.onGesture || !cardRef.current) return;
    const remove = onGesture(cardRef.current, props.onGesture);
    return remove;
  }, [props.onGesture]);

  return (
    <div
      className={`${styles.Card} ${isPhone() ? styles.CardPhone : ""} ${props.className || ""}`}
      style={props.style}
      onClick={props.onClick}
      onDoubleClick={props.onDoubleClick}
      ref={cardRef}
    >
      <div
        className={`${styles.CardHover} ${props.onCSSHover || ""} ${props.block && styles.na}`}
        style={props.styleInner}
      ></div>

      {props.block && <div className={styles.blockSign}>
        <i className="far fa-times-circle"></i>
        <p>{props.block}</p>
      </div>}

      {!!img && <img
        src={img}
        className={styles.CardDisplay}
        style={props.styleInner}
      />}
      {!img && <div
        className={styles.fallback}
        style={props.styleInner}
      >
        <div>{props.card.number}</div>
        <p>{props.card.sign}</p>
      </div>}
    </div>
  );
};

export default Card;
