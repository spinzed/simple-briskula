import styles from "./Table.module.css";
import Card from "../Card/Card";
import Deck from "../Deck/Deck";
import { isPhone } from "../../utils/client";

interface TableProps {
  delay: number;
  numberOfCards: number;
  briskula: Game.Card | undefined;
  tableCards: Game.Card[];
  lastWon: boolean;
}

const Table: (props: TableProps) => JSX.Element = (props) => {
  return (
    <div className={`${styles.Table} ${isPhone() ? styles.TablePhone : ""}`}>
      <div className={styles.tableCards}>
        <div>
          {props.tableCards.map((card, i) => (
            <Card key={i} card={card} className={i % 2 === 0 ? styles.evenCard : styles.oddCard} />
          ))}
        </div>
      </div>
      <Deck
        delay={props.delay}
        className={styles.cards}
        numberOfCards={props.numberOfCards}
        briskula={props.briskula}
        lastWon={props.lastWon}
      />
    </div>
  );
};

export default Table;
