import Cookies from "js-cookie";

// Cookies.get returna string vrijednost ako je nade, undefined ako je ne nade

// ako key postoji onda ce vratit true ili false, ako ne onda undefined
export function isAllowed(): (boolean | undefined) {
  const cookie = Cookies.get("allowed")
  if (cookie === "true") return true;
  if (cookie === "false") return false; 
  return undefined;
}

export function setIsAllowed(state: boolean) {
  Cookies.set("allowed", String(state))
}

// ako je name settan returnat ce ga, a ako ne postoji returnat ce undefined
export function savedName(): string | undefined {
  return Cookies.get("name");
}

export function setSavedName(name: string) {
  if (isAllowed()) Cookies.set("name", name);
}

// oce li se prikazat faq. Ako nije setano ili je true prikazi, ako ako je false nemoj
export function showFAQ() {
  const cookie = Cookies.get("showfaq");
  if (cookie === "false") return false; 
  return true;
}

export function setShowFAQ(state: boolean) {
  Cookies.set("showfaq", String(state));
}

// odredi jer odredeni mod enablan ili disablan, true je samo ako je setan kao true
export function getModeEnabled(modeName: string): boolean {
  if (Cookies.get("mode" + modeName) === "true") return true;
  return false;
}

export function setModeEnabled(modeName: string, enabled: boolean) {
  Cookies.set("mode" + modeName, String(enabled));
}

export function getGameIDAndKey(): [string | undefined, string | undefined] {
  const id = Cookies.get("gid") || undefined;
  const key = Cookies.get("gkey") || undefined;
  return [id, key]
}

export function setKeyAndGameID(key: string, id: string) {
  Cookies.set("gid", id);
  Cookies.set("gkey", key);
}

export function resetKeyAndGameID() {
  Cookies.set("gid", "");
  Cookies.set("gkey", "");
}
