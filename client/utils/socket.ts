import { Socket } from "socket.io-client";
import { DefaultEventsMap } from "@socket.io/component-emitter";

// type for the ping function
type GetPing = (io: Socket<DefaultEventsMap, DefaultEventsMap>, callback: (num: number | undefined) => void) => void;

/* NOTE: I could've used the pong event instead of ping, they are the completely same */

// I made it callback based, but I could of made it Promise based
export const ping: GetPing = (io, callback) => {
  // start the performance benchmark
  const t0 = performance.now();

  // timeout function in case the server doesnt respond in time
  const tmt = setTimeout(() => {
    // return undefined and remove all listeners/timeouts
    callback(undefined);
    clearTimeout(tmt);
    io.off("pong");
  }, 5000)

  // ping event listener

  io.on("pong", () => {
    const t1 = performance.now();

    // calculate the time taken, return it via callback
    callback(Math.round((t1 - t0)));

    // remove all listeners/timeouts
    io.off("pong");
    clearTimeout(tmt);
  });

  // emit ping event to the server (after the listener)
  io.emit("ping");
};
