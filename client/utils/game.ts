import { Modes } from "../game/constants";

export function game(): Game {
  if (window.location.host.includes("treseta") || window.location.port == "8080") return "treseta";
  return "briskula";
}

export function standardGameName(): "Briscola" | "Tressette" {
  if (game() === "treseta") return "Tressette";
  return "Briscola";
}

export function relevantModes() {
  return Object.values(Modes).filter(m => m.game === game());
}


interface Color {
  rgb: string;
  rgba: (a: number) => string;
  r: number;
  g: number;
  b: number;
}

interface Theme {
  background: Color;
  darkBackground: Color;
  vibrant: Color;
}

function newColor(r: number, g: number, b: number) {
  return {
    rgb: `rgba(${r}, ${g}, ${b})`,
    rgba: (a: number) => `rgba(${r}, ${g}, ${b}, ${a})`,
    r: r,
    g: g,
    b: b,
  }
}

const briskulaTheme = {
  background: newColor(94, 27, 27),
  darkBackground: newColor(56, 8, 8),
  vibrant: newColor(189, 41, 11)
}

const tresetaTheme = {
  background: newColor(20, 61, 2),
  darkBackground: newColor(12, 33, 2),
  vibrant: newColor(8, 110, 38)
}

export const theme: Theme = game() === "briskula" ? briskulaTheme : tresetaTheme;
