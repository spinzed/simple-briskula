export function isPhone() {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];

    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
}

export function isFullscreen() {
  return document.fullscreenEnabled && !!document.fullscreenElement;
}

export function isPWA() {
  return window.matchMedia('(display-mode: standalone)').matches;
}

export function orientation() {
  if (screen.orientation) {
    if (screen.orientation.type.includes("landscape")) return "landscape";
  } else {
    if (screen.availWidth > screen.availHeight) return "landscape";
  }
  return "portrait";
}
