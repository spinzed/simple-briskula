export function teammates(data: Data.General): string[] {
  return data.players.filter(p => p.isTeammate).map(p => p.name);
}

export function friendlies(data: Data.General): string[] {
  return [data.name, ...data.players.filter(p => p.isTeammate).map(p => p.name)];
}

export function opponents(data: Data.General): string[] {
  return data.players.filter(p => !p.isTeammate).map(p => p.name);
}

export function secondsSince(since: number): number {
  return Math.floor((Date.now() - since) / 1000);
}

export function capitalFirst(string: string): string {
  return string.charAt(0).toLocaleUpperCase() + string.slice(1);
}

const numberVals = {
  "4": 4,
  "5": 5,
  "6": 6,
  "7": 7,
  "F": 8,
  "K": 9,
  "R": 10,
  "A": 11,
  "2": 12,
  "3": 13,
};

export function tresetaSort(cards: Game.Card[], signs: Game.CardSign[]): Game.Card[] {
  return cards.sort((c1, c2) => {
    const i1 = signs.indexOf(c1.sign);
    const i2 = signs.indexOf(c2.sign);
    if (i1 > i2) return 1;
    if (i1 < i2) return -1;
    if (!c1.number) return 1;
    if (!c2.number) return -1;
    if (numberVals[c1.number] > numberVals[c2.number]) return -1;
    if (numberVals[c1.number] < numberVals[c2.number]) return 1;
    return 0;
  })
}
