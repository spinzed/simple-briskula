type Gesture = "up" | "down" | "left" | "right" | "tap";

export function onGesture(element: HTMLElement, listener: (g: Gesture) => void) {
  let startX: number, startY: number;

  // listeners
  const listenerStart = function (event: TouchEvent) {
    startX = event.changedTouches[0].screenX;
    startY = event.changedTouches[0].screenY;
  };
  const listenerEnd = function (event: TouchEvent) {
    const endX = event.changedTouches[0].screenX;
    const endY = event.changedTouches[0].screenY;
    const gesture = getGesture(startX, endX, startY, endY);
    listener(gesture);
  }

  // bind the listeners to the element
  element.addEventListener("touchstart", listenerStart, false);
  element.addEventListener("touchend", listenerEnd, false);

  return () => {
    element.removeEventListener("touchstart", listenerStart);
    element.removeEventListener("touchend", listenerEnd);
  }
}

function getGesture(startX: number, endX: number, startY: number, endY: number): Gesture {
  const diffX = startX - endX;
  const diffY = startY - endY;

  if (Math.abs(diffX) >= Math.abs(diffY) && Math.abs(diffX) > 10) {
    return diffX > 0 ? "left" : "right";
  }
  if (Math.abs(diffY) >= Math.abs(diffX) && Math.abs(diffY) > 10) {
    return diffY > 0 ? "up" : "down";
  }
  return "tap";
}
