// I get mental breakdown when I see this file

// back side
import back from "../static/images/sprites/back.webp";

// bati
import batiA from "../static/images/sprites/batiA.webp";
import bati2 from "../static/images/sprites/bati2.webp";
import bati3 from "../static/images/sprites/bati3.webp";
import bati4 from "../static/images/sprites/bati4.webp";
import bati5 from "../static/images/sprites/bati5.webp";
import bati6 from "../static/images/sprites/bati6.webp";
import bati7 from "../static/images/sprites/bati7.webp";
import batiF from "../static/images/sprites/batiF.webp";
import batiK from "../static/images/sprites/batiK.webp";
import batiR from "../static/images/sprites/batiR.webp";

// dinari
import dinariA from "../static/images/sprites/dinariA.webp";
import dinari2 from "../static/images/sprites/dinari2.webp";
import dinari3 from "../static/images/sprites/dinari3.webp";
import dinari4 from "../static/images/sprites/dinari4.webp";
import dinari5 from "../static/images/sprites/dinari5.webp";
import dinari6 from "../static/images/sprites/dinari6.webp";
import dinari7 from "../static/images/sprites/dinari7.webp";
import dinariF from "../static/images/sprites/dinariF.webp";
import dinariK from "../static/images/sprites/dinariK.webp";
import dinariR from "../static/images/sprites/dinariR.webp";

// kupe
import kupeA from "../static/images/sprites/kupeA.webp";
import kupe2 from "../static/images/sprites/kupe2.webp";
import kupe3 from "../static/images/sprites/kupe3.webp";
import kupe4 from "../static/images/sprites/kupe4.webp";
import kupe5 from "../static/images/sprites/kupe5.webp";
import kupe6 from "../static/images/sprites/kupe6.webp";
import kupe7 from "../static/images/sprites/kupe7.webp";
import kupeF from "../static/images/sprites/kupeF.webp";
import kupeK from "../static/images/sprites/kupeK.webp";
import kupeR from "../static/images/sprites/kupeR.webp";

// spade
import spadeA from "../static/images/sprites/spadeA.webp";
import spade2 from "../static/images/sprites/spade2.webp";
import spade3 from "../static/images/sprites/spade3.webp";
import spade4 from "../static/images/sprites/spade4.webp";
import spade5 from "../static/images/sprites/spade5.webp";
import spade6 from "../static/images/sprites/spade6.webp";
import spade7 from "../static/images/sprites/spade7.webp";
import spadeF from "../static/images/sprites/spadeF.webp";
import spadeK from "../static/images/sprites/spadeK.webp";
import spadeR from "../static/images/sprites/spadeR.webp";

const SPRITES = {
    back,

    batiA,
    bati2,
    bati3,
    bati4,
    bati5,
    bati6,
    bati7,
    batiF,
    batiK,
    batiR,

    dinariA,
    dinari2,
    dinari3,
    dinari4,
    dinari5,
    dinari6,
    dinari7,
    dinariF,
    dinariK,
    dinariR,
    
    kupeA,
    kupe2,
    kupe3,
    kupe4,
    kupe5,
    kupe6,
    kupe7,
    kupeF,
    kupeK,
    kupeR,

    spadeA,
    spade2,
    spade3,
    spade4,
    spade5,
    spade6,
    spade7,
    spadeF,
    spadeK,
    spadeR,
}

const Sprites = Object.freeze(SPRITES)

export default Sprites;
