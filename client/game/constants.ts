export const TEAMMATE_CARDS_TIME = 6000; // unused
export const OPPONENT_CARD_TIME = 4000;
export const MAX_CHAT_MESSAGES = 8;
export const SIGN_SORT: Game.CardSign[] = ["bati", "dinari", "kupe", "spade"];

interface Modes {
  [name: string]: Mode;
}

export const Modes: Modes = {
  BriskSingle1v1: {
    name: "brisk-1v1",
    showName: "1v1",
    game: "briskula",
    maxPlayers: 2,
    maxTeams: 2,
  },
  BriskDouble1v1: {
    name: "brisk-1v1-dupla",
    showName: "Double 1v1",
    game: "briskula",
    maxPlayers: 4,
    maxTeams: 2,
  },
  BriskSingle2v2: {
    name: "brisk-2v2",
    showName: "2v2",
    game: "briskula",
    maxPlayers: 4,
    maxTeams: 2,
  },
  TresSingle1v1: {
    name: "tres-1v1",
    showName: "1v1",
    game: "treseta",
    maxPlayers: 2,
    maxTeams: 2,
  },
  TresSingle2v2: {
    name: "tres-2v2",
    showName: "2v2",
    game: "treseta",
    maxPlayers: 4,
    maxTeams: 2,
  },
};

export const getByModeName = (name: string): (Mode | undefined) => {
  return Object.values(Modes).find(m => m.name === name);
}
