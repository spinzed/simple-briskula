import ReactDOM from 'react-dom/client'
import App from "./App";
import "./style.css";
import { isPhone } from "./utils/client";
import * as worker from "./serviceWorkerRegistration";

console.log("test")

// Register the service worker (for the PWA).
worker.register();

// Set the default font size for phones since the media query doesn't work
// for some devices
if (isPhone()) document.documentElement.style.fontSize = "9px";

ReactDOM.createRoot(document.getElementById('root')!).render(
  <App />
)


