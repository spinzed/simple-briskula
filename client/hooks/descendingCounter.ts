import { useState, useEffect } from "react";

// descending card tracker
export default function useDescendingCounter(newNumber: number) {
  const [activeNumber, setActiveNumber] = useState(newNumber);

  // props
  const updated = newNumber;

  useEffect(() => {
    // in case no. of cards raises
    if (updated > activeNumber) {
      setActiveNumber(updated);
    }
  });

  const updateTracker = (num?: number | undefined) => {
    if (typeof num === "number") {
      setActiveNumber(activeNumber - num);
    } else {
      setActiveNumber(activeNumber - 1);
    }
  }

  return [activeNumber, updateTracker] as const;
}
