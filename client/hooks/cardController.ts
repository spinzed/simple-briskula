import { useState, useRef, useEffect } from "react";

export interface CardAndStatus {
  card: Game.Card,
  status: "shown" | "hidden" | "remove"
}

export default function useCardController(passedCards: Game.Card[]) {
  // function is passed as inital value so that the value is not set multiple times,
  // minor optimisation which is not required
  const [shownCards, setShownCards] = useState<CardAndStatus[]>(() => {
    return passedCards.map(c => {
      return { card: c, status: "shown" }
    });
  })

  const lastCards = useRef(passedCards);
  const newCards = passedCards;

  useEffect(() => {
    // remove all items that have status "remove"
    let removed = 0;
    [...shownCards].forEach((v, i) => {
      if (v.status === "remove") {
        shownCards.splice(i-removed, 1);
        removed++;
      }
    })

    // check if passed in cards array has changed, if it didn't, don't continue
    if (lastCards.current === passedCards) return;
    lastCards.current = passedCards;

    const updated = [...shownCards];

    // map of strings, bati1, dinari5, spadeK
    const updatedCards = updated.map(v => v.card.sign + (v.card.id ? v.card.id : v.card.number));
    const newCardsString = newCards.map(c => c.sign + (c.id ? c.id : c.number));

    // test for additional cards
    newCards.forEach((card, i) => {
      if (!inArr(updatedCards, card)) {
        // insert the card in the new array in the same index as in the original array
        updated.splice(i, 0, { card, status: "shown" });
      }
    });
    // test for removed cards
    updated.forEach(v => {
      if (!inArr(newCardsString, v.card)) {
        v.status = "hidden";
        // after 500 ms, remove it from the array. Can cause problems because
        // closures but whatever fuck it
        setTimeout(() => {
          v.status = "remove"
        }, 300);
      }
    });
    setShownCards(updated);
  });

  const updateCardStatus = (card: Game.Card) => {
    const updated = [...shownCards];
    const val = updated.find(v => same(v.card, card));
    if (val) {
      val.status = "hidden";
      setTimeout(() => {
        val.status = "remove";
      }, 300);
    }
  }

  return [shownCards, updateCardStatus] as const;
}

function same(c1: Game.Card, c2: Game.Card): boolean {
  if (c1.id || c2.id) return c1.sign === c2.sign && c1.id === c2.id;
  return c1.sign === c2.sign && c1.number === c2.number;
}

function inArr(arr: string[], card: Game.Card): boolean {
  return arr.includes(card.sign + (card.id ? card.id : card.number)); 
}
