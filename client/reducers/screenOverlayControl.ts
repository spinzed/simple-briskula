import { Reducer } from "react";

export interface ScreenOverlayState {
  currentScreen: string;
  transType: string | undefined;
  transStatus: string;
  nextScreen: string | undefined;
}

export interface ScreenOverlayAction {
  type: string;
  screen?: string;
  transType?: string;
  nextPhase?: string;
}

export const ScreenOverlayReducer: Reducer<ScreenOverlayState, ScreenOverlayAction> = (state: ScreenOverlayState, action: ScreenOverlayAction) => {
  switch (action.type) {
    case "switchScreen":
      return Object.assign(state, {
        currentScreen: screen
      });

    // start
    case "startTransition":
      return Object.assign({ ...state }, {
        transType: action.transType,
        transStatus: "inProgress",
        nextScreen: action.nextPhase
      });

    // switch the screens
    case "switchTransition":
      return Object.assign({ ...state }, {
        currentScreen: state.nextScreen,
        nextScreen: undefined
      });

    // start the end process
    case "startEndTransition":
      return Object.assign({ ...state }, {
        transStatus: "retracting"
      });

    // finalise the end process
    case "endTransition":
      return Object.assign({ ...state }, {
        transType: undefined,
        transStatus: "idle"
      });

    default:
      console.error("unexpected treducer action type", action.type);
      return state;
  }
}