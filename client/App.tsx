import { useReducer, useState } from "react";
import { ScreenOverlayState, ScreenOverlayReducer } from "./reducers/screenOverlayControl";
import { theme } from "./utils/game";
import Game from "./components/Game/Game";
import JoinMenu from "./components/JoinMenu/JoinMenu";
import PhoneCheck from "./components/PhoneCheck/PhoneCheck";
import StatsOverlay from "./components/StatsOverlay/StatsOverlay";
import EndgameScreen from "./components/EndgameScreen/EndgameScreen";
import GameStartOverlay from "./components/GameStartOverlay/GameStartOverlay";
import { isPhone } from "./utils/client";
import { SOCK, SocketCtx } from "./socket/socket";
import { resetKeyAndGameID, setKeyAndGameID } from "./utils/cookie";

const App = (): JSX.Element => {
  // podatci o game UUID, mom imenu i oponent imenu
  const [gameData, setGameData] = useState<Data.General | undefined>(undefined);
  // podatci o game state (won/lost/draw) i broj bodova
  const [gameEndData, setEndData] = useState<Data.GameEnd | undefined>(undefined);
  // socket context
  const [socket] = useState(SOCK);

  // stanje overlaya
  // -- currentScreen:
  // -- transType
  // -- transStatus
  // -- nextScreen
  const initialState: ScreenOverlayState = {
    currentScreen: "loading",
    transType: undefined,
    transStatus: "idle",
    nextScreen: undefined
  };

  const [screenShowData, reduceScreenShowData] = useReducer(ScreenOverlayReducer, initialState, () => initialState);

  // osnovna funkcija za triggeranje transitiona. Type je koji transition ocemo triggerat, nextPhase
  // je sto ocemo trigerrat nakon toga i delay je koliko dugo ocemo da traje transition DOKAD POCNE NESTAJAT
  const triggerTransition = (type: string, nextPhase: string, delay: number) => {
    reduceScreenShowData({ type: "startTransition", transType: type, nextPhase });

    setTimeout(() => {
      reduceScreenShowData({ type: "startEndTransition" });
    }, delay);
  }

  const handleGameStart = (data: Data.General) => {
    setGameData(data);
    setKeyAndGameID(data.key, data.uuid);
    // type start, nextPhase playing, delay 4000
    triggerTransition("start", "playing", 4000);
  }

  const handleGameEnd = (result: Data.GameEnd) => {
    setEndData(result);
    resetKeyAndGameID();
    triggerTransition("endgame", "endgame", 2000);
  }

  const getToMainMenu = () => {
    triggerTransition("endgame", "menuScreen", 2000);
    // socket.refresh();

    setTimeout(() => {
      setEndData(undefined);
      setGameData(undefined);
    }, 1000);
  }

  /* 
    Ispod se samo odabire trenutni SCREEN i OVERLAY ovisno o screenShowData reduceru.
  */

  let screen: JSX.Element | null = null;
  let overlay: JSX.Element | null = null;

  switch (screenShowData.currentScreen) {
    case "menuScreen":
    case "loading":
      screen = <JoinMenu onGameReady={handleGameStart} />;
      break;
    case "playing":
      screen = <Game data={gameData} transitionStartDelay={4000} onMainMenu={getToMainMenu} onGameEnd={handleGameEnd} />;
      break;
    case "endgame":
      screen = <EndgameScreen data={gameEndData} onMainMenu={getToMainMenu} onRestartGame={() => gameData && handleGameStart(gameData)}/>;
      break;
    default:
      console.error("unexpected game state", screenShowData.currentScreen);
      screen = null;
  }

  switch (screenShowData.transType) {
    case "start":
      overlay = <GameStartOverlay
        in={screenShowData.transStatus === "inProgress"}
        data={gameData}
        onEntered={() => reduceScreenShowData({ type: "switchTransition" })}
        onExited={() => reduceScreenShowData({ type: "endTransition" })}
      />
      break;
    case "endgame":
      overlay = <StatsOverlay
        in={screenShowData.transStatus === "inProgress"}
        onEntered={() => reduceScreenShowData({ type: "switchTransition" })}
        onExited={() => reduceScreenShowData({ type: "endTransition" })}
      />
      break;
    case undefined:
      overlay = null;
      break;
    default:
      console.error("Unexpected overlay type", screenShowData.transType);
      overlay = null;
  }

  const st = {
    backgroundColor: theme.background.rgb,
    "--bg": theme.background.rgb,
    "--bgr": theme.background.r,
    "--bgg": theme.background.g,
    "--bgb": theme.background.b,
  }

  return (
    <div id="App" style={st}>
      <SocketCtx.Provider value={socket}>
        {isPhone() && <PhoneCheck />}
        {overlay}
        {screen}
      </SocketCtx.Provider>
    </div>
  );
};

export default App;
